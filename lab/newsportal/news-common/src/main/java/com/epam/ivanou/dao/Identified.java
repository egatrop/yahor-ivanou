package com.epam.ivanou.dao;

import com.epam.ivanou.entity.Author;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.entity.News;
import com.epam.ivanou.entity.Tag;

/**
 * The utility interface is implemented by entity classes
 * to provide them with getter method for the identifier .
 *
 * @see Author
 * @see Comment
 * @see News
 * @see Tag
 */
public interface Identified {

    /**
     * Returns the identifier of the entity.
     *
     * @return the identifier of the entity
     */
    Long getId();
}
