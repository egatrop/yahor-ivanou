package com.epam.ivanou.dao;

import com.epam.ivanou.exception.DAOException;

import java.util.List;

/**
 * Unified interface to manage persistent data.
 * Provides basic CRUD operations.
 *
 * @param <T>  type of persistent object
 */

public interface  IGenericDAO<T extends Identified> {

    /**
     * Creates a item in the data source appropriate to the passed object.
     *
     * @param object the object to create
     * @return the object appropriate to the created item
     * @throws DAOException
     */
    T create(T object) throws DAOException;

    /**
     * Returns the object appropriate to the item in the data source
     * with primary key equals to passed key.
     *
     * @param key the value of the primary key
     * @return the object appropriate to the primary key
     * @throws DAOException
     */
    T getByPK(Long key) throws DAOException;

    /**
     * Updates the item in the data source appropriate to the passed object.
     *
     * @param object the object to update
     * @throws DAOException
     */
    void update(T object) throws DAOException;

    /**
     * Removes the item from the data source appropriate to the primary key.
     *
     * @param key the primary key of the entity
     * @throws DAOException
     */
    void delete(Long key) throws DAOException;

    /**
     * Returns the list of all the objects appropriate to all the items of the entity
     * in the data source.
     *
     * @return the list of all the objects
     * @throws DAOException
     */
    List<T> getAll() throws DAOException;
}
