package com.epam.ivanou.service.impl.jdbc;

import com.epam.ivanou.dao.ICommentDAO;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.service.ICommentService;
import org.apache.log4j.Logger;

import java.util.List;

public class CommentJDBCService implements ICommentService {

    private static final Logger logger = Logger.getLogger(CommentJDBCService.class);

    private ICommentDAO commentDAO;

    public CommentJDBCService(ICommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    @Override
    public Comment get(Long commentId) throws ServiceException {
        Comment comment;

        try {
            comment = commentDAO.getByPK(commentId);
            if (comment == null) throw new ServiceException("No such comment in the database");
        } catch (DAOException e) {
            logger.error("Exception when trying to retrieve the comment with id = "
                    + commentId + ".");
            throw new ServiceException(e);
        }
        return comment;
    }

    @Override
    public Long add(Comment comment) throws ServiceException {
        Long id;
        try {
            id = commentDAO.create(comment).getId();
            logger.info("The comment with id = " + id + " was added.");
        } catch (DAOException e) {
            logger.error("Exception when trying to add the comment to DB.", e);
            throw new ServiceException(e);
        }
        return id;
    }

    @Override
    public void delete(Long commentId) throws ServiceException {
        try {
            commentDAO.delete(commentId);
            logger.info("The comment with id = " + commentId + " was deleted.");
        } catch (DAOException e) {
            logger.error("Exception when trying to delete the comment from DB.", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public int deleteAllByNewsId(Long newsId) throws ServiceException {
        int count;

        try {
            count = commentDAO.deleteAllByNewsId(newsId);
            logger.info("All the comments appropriate to the news' id = "
                    +  newsId + " were deleted.");
        } catch (DAOException e) {
            logger.error("Exception when trying to delete all comments"
                    + " appropriate to the news' id = " + newsId + " from DB.", e);
            throw new ServiceException(e);
        }
        return count;
    }

    @Override
    public List<Comment> getAllByNewsId(Long newsId) throws ServiceException {
        List<Comment> list;

        try {
            list = commentDAO.getAllByNewsId(newsId);
        } catch (DAOException e) {
            logger.error("Exception when trying to retrieve"
                    + " the list of comments appropriate to the news' id = "
                    + newsId + ".", e);
            throw new ServiceException(e);
        }
        return list;
    }
}
