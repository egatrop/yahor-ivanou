package com.epam.ivanou.service;


import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.exception.ServiceException;

import java.util.List;

/**
 * The interface to manage the <code>Comment</code> entity.
 */
public interface ICommentService {

    /**
     * Returns the <code>Comment</code> object appropriate to the
     * Comment item with the id equal to the passed <code>commentId</code> parameter.
     *
     * @param commentId the id of the Comment item
     * @return the Comment object appropriate to the passed id
     * @throws ServiceException
     */
    Comment get(Long commentId) throws ServiceException;

    /**
     * Adds the Comment item appropriate to the passed <code>Comment</code> object.
     *
     * @param comment Comment object
     * @return auto-generated identifier of the inserted Comment item
     * @throws ServiceException
     */
    Long add(Comment comment) throws ServiceException;

    /**
     * Removes the Comment item appropriate to the passed identifier.
     *
     * @param commentId the identifier of the Comment item to delete
     * @throws ServiceException
     */
    void delete(Long commentId) throws ServiceException;

    /**
     * Removes all the Comment items appropriate to the
     * passed News item's identifier.
     *
     * @param newsId the identifier of the News item
     * @throws ServiceException
     */
    int deleteAllByNewsId(Long newsId) throws ServiceException;

    /**
     * Returns the list of the <code>Comment</code> objects appropriate
     * to the <code>News</code> object.
     *
     * @param newsId the identifier of the News object
     * @return the list of the Comment objects
     * @throws ServiceException
     */
    List<Comment> getAllByNewsId(Long newsId) throws ServiceException;
}
