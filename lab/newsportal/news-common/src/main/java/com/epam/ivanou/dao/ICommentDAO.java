package com.epam.ivanou.dao;


import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.entity.Comment;

import java.util.List;

/**
 * The interface to manage the <code>Comment</code> entity.
 */
public interface ICommentDAO extends IGenericDAO<Comment> {

    /**
     * Deletes all the <code>Comment</code> items associated with the
     * <code>News</code> item.
     *
     * @param newsId the identifier of the news
     * @return the number of deleted comments
     * @throws DAOException
     */
    int deleteAllByNewsId(Long newsId) throws DAOException;

    /**
     * Returns the list of the <code>Comment</code> objects associated
     * with the <code>News</code> object.
     *
     * @param newsId the identifier of the news
     * @return the list of the comments
     * @throws DAOException
     */
    List<Comment> getAllByNewsId(Long newsId) throws DAOException;
}
