package com.epam.ivanou.dao.impl.hibernate;

import com.epam.ivanou.dao.INewsDAO;
import com.epam.ivanou.entity.News;
import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.utils.SearchCriteria;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.*;

import java.util.List;
import java.util.Set;

public class NewsJPADAO implements INewsDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int countAll() throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List newsList = session.createCriteria(News.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
        int size = newsList.size();
        tx.commit();
        session.close();
        return size;
    }

    @Override
    public News create(News object) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(object);
        tx.commit();
        session.close();
        return object;
    }

    @Override
    public News getByPK(Long key) throws DAOException {
        Session session = this.sessionFactory.openSession();
        News news = (News) session.get(News.class, key);
        session.close();
        return news;
    }

    @Override
    public void update(News object) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(object);
        tx.commit();
        session.close();
    }

    @Override
    public void delete(Long key) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        News news = (News) session.get(News.class, key);
        session.delete(news);
        tx.commit();
        session.close();
    }

    @Override
    public List<News> getAll() throws DAOException {
        Session session = this.sessionFactory.openSession();
        Criteria criteria = session.createCriteria(News.class)
                .addOrder(Order.desc("commentsCount"))
                .addOrder(Order.desc("modificationDate"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.setFetchMode("comments", FetchMode.SELECT);
        Transaction tx = session.beginTransaction();
        List newsList = criteria.list();
        tx.commit();
        session.close();
        return newsList;
    }


    @Override
    @SuppressWarnings("unchecked")
    public List<News> search(SearchCriteria searchCriteria)
            throws DAOException {
        List<News> newsList;
        Session session = this.sessionFactory.openSession();
        Criteria criteria = createSearchCriteria(searchCriteria, session);
        newsList = criteria.list();
        session.close();
        return newsList;
    }

    private Criteria createSearchCriteria(SearchCriteria searchCriteria,
                                          Session session) {
        Long authorId = null;
        Set<Long> tagsId = null;
        if (searchCriteria != null) {
            authorId = searchCriteria.getAuthorId();
            tagsId = searchCriteria.getTagsIdSet();
        }
        Conjunction conjunction = Restrictions.conjunction();
        Criteria criteria = session.createCriteria(News.class);

        if (authorId != null) {
            criteria.createAlias("authors", "author");
            conjunction.add(Restrictions.eq("author.id", authorId));
        }
        if (tagsId != null && tagsId.size() != 0) {
            criteria.createAlias("tags", "tag");
            conjunction.add(Restrictions.in("tag.id", tagsId));
        }
        criteria.add(conjunction);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.addOrder(Property.forName("commentsCount").desc());
        criteria.addOrder(Property.forName("modificationDate").desc());
        return criteria;
    }
}
