package com.epam.ivanou.service.impl.jdbc;

import com.epam.ivanou.dao.INewsDAO;
import com.epam.ivanou.entity.News;
import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.service.INewsService;
import com.epam.ivanou.utils.SearchCriteria;
import org.apache.log4j.Logger;

import java.util.List;

public class NewsJDBCService implements INewsService {

    private static final Logger logger = Logger.getLogger(NewsJDBCService.class);

    private INewsDAO newsDAO;

    public NewsJDBCService(INewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    @Override
    public Long add(News news) throws ServiceException {
        Long id;

        try {
            id = newsDAO.create(news).getId();
            logger.info("The news with id = " + id + " was added.");
        } catch (DAOException e) {
            logger.error("Exception when trying to add the news to DB.", e);
            throw new ServiceException(e);
        }
        return id;
    }

    @Override
    public void update(News news) throws ServiceException {
        try {
            newsDAO.update(news);
            logger.info("The news with id = " + news.getId()
                    + " was updated. Current parameters : "
                    + "title = " + news.getTitle() + ", short text = "
                    + news.getShortText() + ", full text = " + news.getFullText()
                    + ",  modification date = " + news.getModificationDate() + "." );
        } catch (DAOException e) {
            logger.error("Exception when trying to update the news"
                    + " with id = " + news.getId() + ".", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(Long newsId) throws ServiceException {
        try {
            newsDAO.delete(newsId);
            logger.info("The news with id = " + newsId + " was deleted.");
        } catch (DAOException e) {
            logger.error("Exception when trying to delete the news"
                    + " with id = " + newsId + ".", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public News get(Long newsId) throws ServiceException {
        News news;

        try {
            news = newsDAO.getByPK(newsId);
            if (news == null) throw new ServiceException("No such news in the database");
        } catch (DAOException e) {
            logger.error("Exception when trying to retrieve the news with id = "
                    + newsId + ".");
            throw new ServiceException(e);
        }
        return news;
    }

    @Override
    public List<News> getAll() throws ServiceException {
        List<News> list;

        try {
            list = newsDAO.getAll();
        } catch (DAOException e) {
            logger.error("Exception when trying to retrieve all the news.");
            throw new ServiceException(e);
        }
        return list;
    }

    @Override
    public List<News> search(SearchCriteria criteria) throws ServiceException {
        List<News> list;

        try {
            list = newsDAO.search(criteria);
        } catch (DAOException e) {
            logger.error("Exception when trying to find the news appropriate " +
                    "to given search criteria.");
            throw new ServiceException(e);
        }
        return list;
    }

    @Override
    public int countAll() throws ServiceException {
        try {
            return newsDAO.countAll();
        } catch (DAOException e) {
            logger.error("Exception when trying to count all the news.");
            throw new ServiceException(e);
        }
    }
}
