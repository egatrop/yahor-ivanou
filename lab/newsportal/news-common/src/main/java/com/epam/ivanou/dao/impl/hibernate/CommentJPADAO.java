package com.epam.ivanou.dao.impl.hibernate;

import com.epam.ivanou.dao.ICommentDAO;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.exception.DAOException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import java.util.List;

public class CommentJPADAO implements ICommentDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Comment create(Comment object) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(object);
        tx.commit();
        session.close();
        return object;
    }

    @Override
    public Comment getByPK(Long key) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Comment comment = (Comment) session.get(Comment.class, key);
        session.close();
        return comment;
    }

    @Override
    public void update(Comment object) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(object);
        tx.commit();
        session.close();
    }

    @Override
    public void delete(Long key) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Comment comment = (Comment) session.get(Comment.class, key);
        comment.setId(key);
        session.delete(comment);
        tx.commit();
        session.close();
    }

    @Override
    public List<Comment> getAll() throws DAOException {
        //NOP
        throw new UnsupportedOperationException();
    }

    @Override
    public int deleteAllByNewsId(Long newsId) throws DAOException {
        //NOP
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Comment> getAllByNewsId(Long newsId) throws DAOException {
        //NOP
        throw new UnsupportedOperationException();
    }
}
