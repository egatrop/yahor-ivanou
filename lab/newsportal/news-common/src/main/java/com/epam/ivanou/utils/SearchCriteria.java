package com.epam.ivanou.utils;


import com.epam.ivanou.dao.INewsDAO;
import com.epam.ivanou.service.impl.jdbc.NewsJDBCService;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * This utility class is used to pass the search parameters
 * to the <code>search</code> methods of INewsService and INewsDAO
 * implementations .
 *
 * @see INewsDAO
 * @see NewsJDBCService
 */
public class SearchCriteria {

    private Set<Long> tagsIdSet = Collections.emptySet();
    private Long authorId;

    /**
     * Returns the the set of Tag objects' identifiers.
     *
     * @return the set of Tag objects' identifiers
     */
    public Set<Long> getTagsIdSet() {
        return tagsIdSet;
    }

    /**
     * Sets the inner <tt>Set&lt;Long&gt;</tt> property which represents the list of
     * tags' identifiers with the passed set.
     *
     * @param tagsIdSet the set of the Tag objects' identifiers
     */
    public void setTagsIdSet(Set<Long> tagsIdSet) {
        this.tagsIdSet = tagsIdSet;
    }

    /**
     * Adds the Tag object's identifier to the set
     * of the Tag objects' identifiers.
     *
     * @param tagId the Tag object's identifier
     */
    public void addTagId(Long tagId) {
        if (tagsIdSet.isEmpty()) {
            tagsIdSet = new HashSet<>();
        }
        tagsIdSet.add(tagId);
    }

    /**
     * Returns the <code>Author</code> object's identifier.
     *
     * @return the Author object's identifier
     */
    public Long getAuthorId() {
        return authorId;
    }

    /**
     * Sets the inner <code>Long</code> property which represents the identifier of Author item.
     *
     *@param authorId the Author object's identifier
     */
    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }
}
