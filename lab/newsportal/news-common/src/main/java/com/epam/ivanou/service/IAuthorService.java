package com.epam.ivanou.service;


import com.epam.ivanou.entity.Author;
import com.epam.ivanou.exception.ServiceException;

import java.util.List;

/**
 * The interface to manage the <code>Author</code> entity.
 */
public interface IAuthorService {

    /**
     * Returns the <code>Author</code> object appropriate to the
     * Author item with the id equal to the passed <code>authorId</code> parameter.
     *
     * @param authorId the id of the Author item
     * @return the Author object appropriate to the passed id
     * @throws ServiceException
     */
    Author get(Long authorId) throws ServiceException;

    /**
     * Adds a new Author item appropriate to the passed <code>Author</code> object.
     *
     * @param author Author object
     * @return auto-generated id of the inserted Author item
     * @throws ServiceException
     */
    Long add(Author author) throws ServiceException;

    /**
     * Updates the Author item appropriate to the passed <code>Author</code> object.
     *
     * @param author the updated Author object
     * @throws ServiceException
     */
    void update(Author author) throws ServiceException;

    /**
     * Returns the list of all the <code>Author</code> objects appropriate
     * to all the Author items.
     *
     * @return the list of the Author objects
     * @throws ServiceException
     */
    List<Author> getAll() throws ServiceException;

    /**
     * Returns the <code>Author</code> object appropriate
     * to the <code>News</code> object.
     *
     * @param newsId the id of the News object
     * @return the list of the Comment objects
     * @throws ServiceException
     */
    Author getByNewsId(Long newsId) throws ServiceException;

    /**
     * Links the <code>Author</code> object with the <code>News</code> object
     * appropriate to specified parameters.
     *
     *  @param newsId the id of the News object
     *  @param authorId the id of the Author object
     *  @return the number of linked pairs (either 1 if linking was
     *  successful or 0 if not)
     *  @throws ServiceException
     */
    int linkWithNews(Long newsId, Long authorId) throws ServiceException;

    /**
     * Unlinks the <code>Author</code> object with the <code>News</code> object
     * appropriate to specified parameters.
     *
     *  @param newsId the id of the News object
     *  @return the number of unlinked pairs
     *  @throws ServiceException if there is no appropriate pairs to unlink
     */
    int unlinkWithNews(Long newsId) throws ServiceException;

 }
