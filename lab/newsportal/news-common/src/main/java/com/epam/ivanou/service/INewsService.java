package com.epam.ivanou.service;


import com.epam.ivanou.entity.News;
import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.utils.SearchCriteria;

import java.util.List;

/**
 * The interface to manage the <code>News</code> entity.
 *
 * @see SearchCriteria
 */
public interface INewsService {

    /**
     * Adds a new News item appropriate to the passed <code>News</code> object.
     *
     * @param news News object
     * @return auto-generated id of the inserted News item
     * @throws ServiceException
     */
    Long add(News news) throws ServiceException;

    /**
     * Updates the News item appropriate to the passed <code>News</code> object.
     *
     * @param news the updated News object
     * @throws ServiceException
     */
    void update(News news) throws ServiceException;

    /**
     * Removes the News item appropriate to the passed identifier.
     *
     * @param newsId the id of the News item to delete
     * @throws ServiceException
     */
    void delete(Long newsId) throws ServiceException;

    /**
     * Returns the <code>News</code> object appropriate to the
     * News item with the id equal to the passed <code>newsId</code> parameter.
     *
     * @param newsId the id of the News item
     * @return the News object appropriate to the passed id
     * @throws ServiceException
     */
    News get(Long newsId) throws ServiceException;

    /**
     * Returns the list of all the <code>News</code> objects appropriate
     * to all the News items.
     *
     * @return the list of the News objects
     * @throws ServiceException
     */
    List<News> getAll() throws ServiceException;

    /**
     * Returns the list of the <code>News</code> objects appropriate to the search parameters
     * passed through the <code>SearchCriteria</code> object, ordered by
     * most commented <code>News</code> item and modification date.
     *
     * @param criteria the SearchCriteria object with defined parameters
     * @return the list of News objects appropriate to the search parameters
     * @throws ServiceException
     */
    List<News> search(SearchCriteria criteria) throws ServiceException;

    /**
     * Returns the total number of all the <code>News</code> items within
     * the data source.
     *
     * @return the total number of all <code>News</code> items
     * @throws ServiceException
     */
    int countAll() throws ServiceException;
}
