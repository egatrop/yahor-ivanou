package com.epam.ivanou.service;

import com.epam.ivanou.entity.Tag;
import com.epam.ivanou.exception.ServiceException;

import java.util.List;

/**
 * The interface to manage the <code>Tag</code> entity.
 */
public interface ITagService {

    /**
     * Returns the <code>Tag</code> object appropriate to the
     * Tag item with the id equal to the passed <code>tagId</code> parameter.
     *
     * @param tagId the id of the Tag item
     * @return the Tag object appropriate to the passed id
     * @throws ServiceException
     */
    Tag get(Long tagId) throws ServiceException;

    /**
     * Adds a new Tag item appropriate to the passed <code>Tag</code> object.
     *
     * @param tag Tag object
     * @return auto-generated identifier of the inserted Tag item
     * @throws ServiceException
     */
    Long add(Tag tag) throws ServiceException;

    /**
     * Removes the Tag item appropriate to the passed identifier.
     *
     * @param tagId the identifier of the Tag item to delete
     * @throws ServiceException
     */
    void delete(Long tagId) throws ServiceException;

    /**
     * Updates the Tag item appropriate to the passed <code>Tag</code> object.
     *
     * @param tag the updated Tag object
     * @throws ServiceException
     */
    void update(Tag tag) throws ServiceException;

    /**
     * Returns the list of all the <code>Tag</code> objects appropriate to all
     * the Tag items.
     *
     * @return the list of all the Tag objects
     * @throws ServiceException
     */
    List<Tag> getAll() throws ServiceException;

    /**
     * Returns the list of the <code>Tag</code> objects appropriate
     * to the <code>News</code> object.
     *
     * @param newsId the identifier of the News object
     * @return the list of the Tag objects
     * @throws ServiceException
     */
    List<Tag> getAllByNewsId(Long newsId) throws ServiceException;

    /**
     * Links the <code>Tag</code> object with the <code>News</code> object
     * appropriate to specified parameters.
     *
     *  @param newsId the identifier of the News object
     *  @param tagId the identifier of the Tag object
     *  @return the number of linked pairs (either 1 if linking was
     *  successful or 0 if not)
     *  @throws ServiceException
     */
    int linkWithNews(Long newsId, Long tagId) throws ServiceException;

    /**
     * Unlinks the <code>Tag</code> object with the <code>News</code> object
     * appropriate to specified parameters.
     *
     *  @param newsId the identifier of the News object
     *  @return the number of unlinked pairs
     *  @throws ServiceException if there is no appropriate pairs to unlink
     */
    int unlinkWithNews(Long newsId) throws ServiceException;
}
