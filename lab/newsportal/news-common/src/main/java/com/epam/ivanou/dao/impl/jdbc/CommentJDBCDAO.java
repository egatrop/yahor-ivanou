package com.epam.ivanou.dao.impl.jdbc;

import com.epam.ivanou.dao.ICommentDAO;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.utils.DBUtils;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CommentJDBCDAO extends AbstractJDBCDAO<Comment>
        implements ICommentDAO {

    private final static String TABLE_NAME_SQL = "COMMENTS";
    private final static String SEQ_NAME_SQL = "COMMENTS_SEQ";

    private final static String ID_SQL = "CO_COMMENT_ID_PK";
    private final static String NEWS_ID_FK_SQL = "CO_NEWS_ID_FK";
    private final static String TEXT_SQL = "CO_COMMENT_TEXT";
    private final static String DATE_SQL = "CO_CREATION_DATE";

    private final static String SELECT_QUERY_SQL =
            "SELECT " + ID_SQL + ", " + NEWS_ID_FK_SQL
            + ", " + TEXT_SQL + ", " + DATE_SQL + " FROM " + TABLE_NAME_SQL;

    private static final String CREATE_QUERY_SQL =
            "INSERT INTO " + TABLE_NAME_SQL
            + " (" + ID_SQL + ", " + NEWS_ID_FK_SQL + ", " + TEXT_SQL + ", " + DATE_SQL +") "
            + "VALUES (" + SEQ_NAME_SQL + ".NEXTVAL , ?, ?, ?)";

    private static final String DELETE_QUERY_SQL =
            "DELETE FROM " + TABLE_NAME_SQL
            + " WHERE " + ID_SQL + " = ?";

    private static final String UPDATE_QUERY_SQL =
            "UPDATE " + TABLE_NAME_SQL
            + " SET " + TEXT_SQL + " = ?, " + DATE_SQL + " = ?  WHERE " + ID_SQL + " = ?";

    private static final String SELECT_BY_NEWS_ID_SQL =
            "SELECT " + ID_SQL + ", " + NEWS_ID_FK_SQL
            + ", " + TEXT_SQL + ", " + DATE_SQL + " FROM " + TABLE_NAME_SQL
            + " WHERE " + NEWS_ID_FK_SQL + " = ? ORDER BY " + DATE_SQL + " DESC";

    private static final String DELETE_BY_NEWS_ID_SQL =
            "DELETE FROM " + TABLE_NAME_SQL
            + " WHERE " + NEWS_ID_FK_SQL + " = ?";


    private DataSource dataSource;

    public CommentJDBCDAO(DataSource dataSource) {
        super(dataSource);
        this.dataSource = dataSource;
    }

    @Override
    public void delete(Long commentId) throws DAOException {
        super.delete(commentId);
    }

    @Override
    public int deleteAllByNewsId(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        int count  = 0;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(DELETE_BY_NEWS_ID_SQL);
            statement.setLong(1, newsId);
            count = statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return count;
    }

    @Override
    public List<Comment> getAllByNewsId(Long newsId) throws DAOException {
        List<Comment> list;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(SELECT_BY_NEWS_ID_SQL);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            list = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(resultSet);
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return list.isEmpty() ? Collections.emptyList() : list;
    }

    @Override
    protected List<Comment> parseResultSet(ResultSet rs) throws DAOException {
        LinkedList<Comment> result = new LinkedList<>();

        try {
            while (rs.next()) {
                Comment comment = new Comment();
                comment.setId(rs.getLong(1));
                comment.setNewsId(rs.getLong(2));
                comment.setText(rs.getString(3));
                comment.setCreationDate(rs.getTimestamp(4));
                result.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement,
                                             Comment object) throws DAOException {
        try {
            statement.setLong(1, object.getNewsId());
            statement.setString(2, object.getText());
            statement.setTimestamp(3, (Timestamp) object.getCreationDate());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement,
                                             Comment object) throws DAOException {
        try {
            statement.setString(1, object.getText());
            statement.setTimestamp(2, (Timestamp) object.getCreationDate());
            statement.setLong(3, object.getId());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected int[] getColumnIndexes() {
        return new int[]{1, 2, 3, 4};
    }

    @Override
    protected String getSelectQuery() {
        return SELECT_QUERY_SQL;
    }

    @Override
    protected String getCreateQuery() {
        return CREATE_QUERY_SQL;
    }

    @Override
    protected String getUpdateQuery() {
        return UPDATE_QUERY_SQL;
    }

    @Override
    protected String getDeleteQuery() {
        return DELETE_QUERY_SQL;
    }

    @Override
    protected String getIdString() {
        return ID_SQL;
    }
}
