package com.epam.ivanou.service;

import com.epam.ivanou.entity.Author;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.entity.Tag;
import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.utils.NewsValueObject;
import com.epam.ivanou.utils.SearchCriteria;

import java.util.List;

/**
 * The interface to manage all the entities of the Application.
 *
 * @see NewsValueObject
 * @see SearchCriteria
 */
public interface INewsManagementService {

    /**
     * Adds the News item appropriate to the passed <code>NewsValueObject</code> object
     * and links it with Author and Tag items. This operation is transactional and includes
     * access to several entities. If any exception raises the transaction will be
     * rolled back.
     *
     * <p>
     *     Note: The <tt>NewsValueObject</tt> parameter should contain fully instantiated <tt>News</tt>
     *     property (except <tt>News#id</tt>), whereas <tt>Author</tt> and <tt>List&lt;Tag&gt;</tt> properties mandatory should
     *     contain only their identifiers.
     * </p>
     * @param valueObject the bundle with News, Author and the list
     *                    of Tag objects
     * @return the auto-generated identifier of the inserted News item
     * @throws ServiceException
     */
    Long addNews(NewsValueObject valueObject) throws ServiceException;

    /**
     * Updates the News item appropriate to the passed <tt>NewsValueObject</tt> object.
     * This operation is transactional and includes access to several entities.
     * If any exception raises the transaction will be rolled back.
     *
     * <p>
     *     Note: The <tt>NewsValueObject</tt> parameter should contain fully instantiated <tt>News</tt>
     *     property (except <tt>News#id</tt>), whereas <tt>Author</tt> and <tt>List&lt;Tag&gt;</tt> properties mandatory should
     *     contain only their identifiers.
     * </p>
     * @param valueObject the bundle with News, Author and the list
     *                    of Tag objects
     * @throws ServiceException
     */
    void editNews(NewsValueObject valueObject) throws ServiceException;

    /**
     * Removes the News items with all Comment items attached to them. This operation
     * is transactional and includes access to several entities. If any exception raises
     * the transaction will be rolled back.
     *
     * @param newsIdList the list of the News items' identifiers
     * @throws ServiceException
     */
    void deleteNewsById(List<Long> newsIdList) throws ServiceException;

    /**Retrieves the News item and the Author, the list of the Comment and the list of the Tag items
     * associated with it. If any exception raises the transaction will be rolled back.
     *
     * <p>
     *     Note: All the properties of the resulting <tt>NewsValueObject</tt> object are fully
     *     instantiated.
     * </p>
     *
     * @param newsId the identifier of the News item
     * @return the NewsValueObject object with fully instantiated properties
     * @throws ServiceException
     */
    NewsValueObject getNewsById(Long newsId) throws ServiceException;

    /**Retrieves the list of the News items and the Author, the list of the Comment
     * and the list of the Tag items associated with them.  If any exception raises
     * the transaction will be rolled back.
     *
     * <p>
     *     Note: All the properties of the resulting <tt>NewsValueObject</tt> object are fully
     *     instantiated.
     * </p>
     *
     * @return the list of the NewsValueObject objects with fully instantiated properties
     * @throws ServiceException
     */
    List<NewsValueObject> getAllNews() throws ServiceException;

    /**Retrieves the list of the News items and the Author, the list of the Comment
     * and the list of the Tag items associated with them appropriate to the passed
     * <tt>SearchCriteria</tt> object.  If any exception raises
     * the transaction will be rolled back.
     *
     * <p>
     *     Note:<br>
     *     1. The resulting list is ordered by the most commented News and modification
     *     date.<br>
     *     2. All the properties of the resulting <tt>NewsValueObject</tt> object are fully
     *     instantiated.
     * </p>
     *
     * @param criteria the SearchCriteria object
     * @return the list of the NewsValueObject objects with fully instantiated properties
     * @throws ServiceException
     */
    List<NewsValueObject> searchNews(SearchCriteria criteria)
            throws ServiceException;

    /**
     *
     * @return the total number of the News items
     * @throws ServiceException
     */
    int countAllNews() throws ServiceException;

    /**
     * Adds a new Author item appropriate to the passed <code>Author</code> object.
     *
     * <p>
     *     Note: The <tt>Author</tt> parameter should be fully instantiated object (except <tt>Author#id</tt>).
     * </p>
     *
     * @param author Author object
     * @return auto-generated id of the inserted Author item
     * @throws ServiceException
     */
    Long addAuthor(Author author) throws ServiceException;

    /**
     * Returns the <code>Author</code> object appropriate to the
     * Author item with the id equal to the passed <code>authorId</code> parameter.
     *
     * @param authorId the id of the Author item
     * @return the Author object appropriate to the passed id
     * @throws ServiceException
     */
    Author getAuthorById(Long authorId) throws ServiceException;

    /**
     * Updates the Author item appropriate to the passed <code>Author</code> object.
     *
     * <p>
     *     Note: The <tt>Author</tt> parameter should be fully instantiated object.
     * </p>
     *
     * @param author the updated Author object
     * @throws ServiceException
     */
    void editAuthor(Author author) throws ServiceException;

    /**
     * Sets the Author item appropriate to the passed <code>Author</code> object the expiration date.
     *
     * <p>
     *     Note: The <tt>Author</tt> parameter should be fully instantiated object.
     * </p>
     *
     * @param author the updated Author object
     * @throws ServiceException
     */
    void expireAuthor(Author author) throws ServiceException;

    /**
     * Returns the list of all the <code>Author</code> objects appropriate
     * to all the Author items.
     *
     * <p>
     *     Note: The result list contains fully instantiated <code>Author</code> objects.
     * </p>
     *
     * @return the list of the Author objects
     * @throws ServiceException
     */
    List<Author> getAllAuthors() throws ServiceException;

    /**
     * Returns the <code>Tag</code> object appropriate to the
     * Tag item with the id equal to the passed <code>tagId</code> parameter.
     *
     * @param tagId the id of the Tag item
     * @return the Tag object appropriate to the passed id
     * @throws ServiceException
     */
    Tag getTagById(Long tagId) throws ServiceException;

    /**
     * Adds a new Tag item appropriate to the passed <code>Tag</code> object.
     *
     * <p>
     *     Note: The <tt>Author</tt> parameter should be fully instantiated object (except <tt>Tag#id</tt>).
     * </p>
     *
     * @param tag Tag object
     * @return auto-generated identifier of the inserted Tag item
     * @throws ServiceException
     */
    Long addTag(Tag tag) throws ServiceException;

    /**
     * Updates the Tag item appropriate to the passed <code>Tag</code> object.
     *
     * <p>
     *     Note: The <tt>Author</tt> parameter should be fully instantiated object.
     * </p>
     *
     * @param tag the updated Tag object
     * @throws ServiceException
     */
    void editTag(Tag tag) throws ServiceException;

    /**
     * Removes the Tag item appropriate to the passed identifier.
     *
     * @param tagId the identifier of the Tag item to delete
     * @throws ServiceException
     */
    void deleteTag(Long tagId) throws ServiceException;

    /**
     * Returns the list of all the <code>Tag</code> objects appropriate to all
     * the Tag items.
     *
     * @return the list of all the Tag objects
     * @throws ServiceException
     */
    List<Tag> getAllTags() throws ServiceException;

    /**
     * Returns the <code>Comment</code> object appropriate to the
     * Comment item with the id equal to the passed <code>commentId</code> parameter.
     *
     * @param commentId the id of the Comment item
     * @return the Comment object appropriate to the passed id
     * @throws ServiceException
     */
    Comment getCommentById(Long commentId) throws ServiceException;

    /**
     * Adds Comment item appropriate to the passed <code>Comment</code> object.
     *
     * <p>
     *     Note: The <tt>Comment</tt> parameter should be fully instantiated object (except <tt>Comment#id</tt>).
     * </p>
     *
     * @param comment Comment object
     * @return auto-generated identifier of the inserted Comment item
     * @throws ServiceException
     */
    Long addComment(Comment comment) throws ServiceException;

    /**
     * Removes the Comment item appropriate to the passed identifier.
     *
     * @param commentId the identifier of the Comment item to delete
     * @throws ServiceException
     */
    void deleteComment(Long commentId) throws ServiceException;
}
