package com.epam.ivanou.service.impl.jdbc;

import com.epam.ivanou.dao.ITagDAO;
import com.epam.ivanou.entity.Tag;
import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.service.ITagService;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class TagJDBCService implements ITagService {

    private static final Logger logger = Logger.getLogger(AuthorJDBCService.class);

    private ITagDAO tagDAO;

    public TagJDBCService(ITagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    @Override
    public Tag get(Long tagId) throws ServiceException {
        Tag tag;
        try {
            tag = tagDAO.getByPK(tagId);
            if (tag == null) throw new ServiceException("No such tag in the database");
        } catch (DAOException e) {
            logger.error("Exception when trying to retrieve the tag with id = "
                    + tagId + ".");
            throw new ServiceException(e);
        }
        return tag;
    }

    @Override
    public Long add(Tag tag) throws ServiceException {
        Long id;

        try {
            id = tagDAO.create(tag).getId();
            logger.info("The tag with id = " + id + " was added.");
        } catch (DAOException e) {
            logger.error("Exception when trying to add the tag to DB.", e);
            throw new ServiceException(e);
        }
        return id;
    }

    @Override
    public void update(Tag tag) throws ServiceException {
        try {
            tagDAO.update(tag);
            logger.info("The tag with id = " + tag.getId()
                    + " was updated. Current parameters : "
                    + "name = " + tag.getName() + ".");
        } catch (DAOException e) {
            logger.error("Exception when trying to update the tag"
                    + " with id = " + tag.getId() + ".", e);
            throw new ServiceException(e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public void delete(Long tagId) throws ServiceException {
        try {
            tagDAO.unLinkWithNewsOnTagDelete(tagId);
            tagDAO.delete(tagId);
            logger.info("The tag with id = " + tagId + " was deleted.");
        } catch (DAOException e) {
            logger.error("Exception when trying to delete the tag"
                    + " with id = " + tagId + ".", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> getAll() throws ServiceException {
        List<Tag> list;

        try {
            list = tagDAO.getAll();
        } catch (DAOException e) {
            logger.error("Exception when trying to retrieve all the news.");
            throw new ServiceException(e);
        }
        return list;
    }

    @Override
    public List<Tag> getAllByNewsId(Long newsId) throws ServiceException {
        List<Tag> list;

        try {
            list = tagDAO.getAllByNewsId(newsId);
        } catch (DAOException e) {
            logger.error("Exception when trying to retrieve"
                    + " the list of tags appropriate to the news' id = "
                    + newsId + ".", e);
            throw new ServiceException(e);
        }
        return list;
    }

    @Override
    public int linkWithNews(Long newsId, Long tagId) throws ServiceException {
        int count;

        try {
            count = tagDAO.linkWithNews(newsId, tagId);
            logger.info("The tag(id = " + tagId + ") was added to the news(id = "
                    + newsId + ").");
        } catch (DAOException e) {
            logger.error("Exception when trying to link the tag with the news. Tag's id = "
                    + tagId + ", news' id = " + newsId + ".", e);
            throw new ServiceException(e);
        }
        return count;
    }

    @Override
    public int unlinkWithNews(Long newsId) throws ServiceException {
        int count;

        try {
            count = tagDAO.unLinkWithNewsOnNewsDelete(newsId);
        } catch (DAOException e) {
            logger.error("Exception when trying to unlink the tags with author. News' id = "
                    + newsId + ".", e);
            throw new ServiceException(e);
        }
        return count;
    }
}
