package com.epam.ivanou.service.impl.hibernate;

import com.epam.ivanou.entity.Author;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.entity.News;
import com.epam.ivanou.entity.Tag;
import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.service.*;
import com.epam.ivanou.utils.NewsValueObject;
import com.epam.ivanou.utils.SearchCriteria;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

public class NewsManagementJPAService implements INewsManagementService {

    private static final Logger logger = Logger.getLogger(NewsManagementJPAService.class);

    private INewsService newsService;
    private ITagService tagService;
    private IAuthorService authorService;
    private ICommentService commentService;

    public NewsManagementJPAService() {
        super();
    }

    @Override
    public Long addNews(NewsValueObject valueObject) throws ServiceException {
        Long id;
        try {
            News news = valueObject.getNews();
            news.getAuthors().add(valueObject.getAuthor());
            news.getTags().addAll(valueObject.getTags());
            news.getComments().addAll(valueObject.getComments());
            id  =newsService.add(news);
            logger.info("The news with id = " + id + " was added.");
        } catch (ServiceException e) {
            logger.error("Exception when trying to add the news to DB.", e);
            throw new ServiceException(e);
        }
        return id;
    }

    @Override
    public void editNews(NewsValueObject newValueObject) throws ServiceException {
        try {
            logger.info("Start of news editing transaction.");
            News news = newValueObject.getNews();
            Long newsId = news.getId();
            Long authorId = newValueObject.getAuthor().getId();
            NewsValueObject currentValueObject = getNewsById(newsId);

            if (!news.equals(currentValueObject.getNews())
                    || !authorId.equals(currentValueObject.getAuthor().getId())
                    || !currentValueObject.getTags().containsAll(newValueObject.getTags())
                    || currentValueObject.getTags().size() != newValueObject.getTags().size()) {
                news.getAuthors().clear();
                news.getAuthors().add(newValueObject.getAuthor());
                news.getTags().clear();
                news.getTags().addAll(newValueObject.getTags());
                newsService.update(news);
            }
            logger.info("News editing transaction was completed successfully.");
        } catch (ServiceException e) {
            logger.error("News editing transaction was not completed.");
            throw e;
        }
    }

    @Override
    public void deleteNewsById(List<Long> newsIdList) throws ServiceException {
        for (Long newsId : newsIdList) {
            try {
                newsService.delete(newsId);
                logger.info("The news with id = " + newsId + " was deleted.");
            } catch (ServiceException e) {
                logger.error("Exception when trying to delete the news"
                        + " with id = " + newsId + ".", e);
                throw new ServiceException(e);
            }
        }
    }

    @Override
    public NewsValueObject getNewsById(Long newsId) throws ServiceException {
        NewsValueObject valueObject;
        try {
            logger.info("Start of news retrieving transaction.");
            valueObject = new NewsValueObject();
            News news = newsService.get(newsId);
            Author author = news.getAuthors().iterator().next();
            List<Tag> tags = new ArrayList<>();
            tags.addAll(news.getTags());
            List<Comment> comments = new ArrayList<>();
            comments.addAll(news.getComments());
            valueObject.setNews(news);
            valueObject.setAuthor(author);
            valueObject.setTags(tags);
            valueObject.setComments(comments);
            logger.info("News retrieving transaction was completed successfully.");
        } catch (ServiceException e) {
            logger.error("News retrieving transaction was not completed.");
            throw e;
        }
        return valueObject;
    }

    @Override
    public List<NewsValueObject> getAllNews() throws ServiceException {
        List<NewsValueObject> valueObjectList;
        try {
            logger.info("Start retrieving of all the news transaction.");
            valueObjectList = new ArrayList<>();
            List<News> newsList = newsService.getAll();
            for (News news : newsList) {
                NewsValueObject valueObject = getNewsById(news.getId());
                valueObjectList.add(valueObject);
            }
            logger.info("Retrieving of all the news transaction was completed successfully.");
        } catch (ServiceException e) {
            logger.error("Retrieving of all the news transaction was not completed.");
            throw e;
        }
        return valueObjectList.size() != 0 ? valueObjectList : Collections.emptyList();
    }

    @Override
    public List<NewsValueObject> searchNews(SearchCriteria criteria) throws ServiceException {
        List<NewsValueObject> valueObjectList;
        try {
            logger.info("Start of news searching transaction.");
            valueObjectList = new ArrayList<>();
            List<News> newsList = newsService.search(criteria);
            for (News news : newsList) {
                NewsValueObject valueObject = getNewsById(news.getId());
                valueObjectList.add(valueObject);
            }
            logger.info("News searching transaction was completed successfully.");
        } catch (ServiceException e) {
            logger.error("News searching transaction was not completed.");
            throw e;
        }
        return valueObjectList.size() != 0 ? valueObjectList : Collections.emptyList();
    }

    @Override
    public int countAllNews() throws ServiceException {
        return newsService.countAll();
    }

    @Override
    public Long addAuthor(Author author) throws ServiceException {
        return authorService.add(author);
    }

    @Override
    public Author getAuthorById(Long authorId) throws ServiceException {
        return authorService.get(authorId);
    }

    @Override
    public void editAuthor(Author author) throws ServiceException {
        authorService.update(author);
    }

    @Override
    public void expireAuthor(Author author) throws ServiceException {
        author.setExpiredDate(Timestamp.valueOf(LocalDateTime.now()));
        editAuthor(author);
    }

    @Override
    public List<Author> getAllAuthors() throws ServiceException {
        return authorService.getAll();
    }

    @Override
    public Tag getTagById(Long tagId) throws ServiceException {
        return tagService.get(tagId);
    }

    @Override
    public Long addTag(Tag tag) throws ServiceException {
        return tagService.add(tag);
    }

    @Override
    public void editTag(Tag tag) throws ServiceException {
        tagService.update(tag);
    }

    @Override
    public void deleteTag(Long tagId) throws ServiceException {
        tagService.delete(tagId);
    }

    @Override
    public List<Tag> getAllTags() throws ServiceException {
        return tagService.getAll();
    }

    @Override
    public Comment getCommentById(Long commentId) throws ServiceException {
        return commentService.get(commentId);
    }

    @Override
    public Long addComment(Comment comment) throws ServiceException {
        return commentService.add(comment);
    }

    @Override
    public void deleteComment(Long commentId) throws ServiceException {
        commentService.delete(commentId);
    }

    @Required
    public void setNewsService(INewsService newsService) {
        this.newsService = newsService;
    }

    @Required
    public void setTagService(ITagService tagService) {
        this.tagService = tagService;
    }

    @Required
    public void setAuthorService(IAuthorService authorService) {
        this.authorService = authorService;
    }

    @Required
    public void setCommentService(ICommentService commentService) {
        this.commentService = commentService;
    }
}
