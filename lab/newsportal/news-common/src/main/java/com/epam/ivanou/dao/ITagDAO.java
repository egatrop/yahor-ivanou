package com.epam.ivanou.dao;


import com.epam.ivanou.entity.Tag;
import com.epam.ivanou.exception.DAOException;

import java.util.List;

/**
 * The interface to manage the <code>Tag</code> entity.
 */
public interface ITagDAO extends IGenericDAO<Tag> {

     /**
      * Links the <code>Tag</code> object with the <code>News</code> object
      * appropriate to specified parameters.
      *
      *  @param newsId the identifier of the News object
      *  @param tagId the identifier of the Tag object
      *  @return the number of linked pairs (either 1 if linking was
      *  successful or 0 if not)
      *  @throws DAOException
      */
     int linkWithNews(Long newsId, Long tagId) throws DAOException;

    /**
     * Unlinks the <code>Tag</code> object with the <code>News</code> object
     * appropriate to specified parameters.
     *
     *  @param newsId the identifier of the News object
     *  @return the number of unlinked pairs
     *  @throws DAOException if there is no appropriate pairs to unlink
     */
     int unLinkWithNewsOnNewsDelete(Long newsId) throws DAOException;

    /**
     * Returns the list of the <code>Tag</code> objects associated
     * with the <code>News</code> object.
     *
     * @param newsId the identifier of the News object
     * @return the list of the Tag objects
     * @throws DAOException
     */
    List<Tag> getAllByNewsId(Long newsId) throws DAOException;

    /**
     * Unlinks the <code>Tag</code> object with the <code>News</code> object
     * appropriate to specified parameters.
     *
     *  @param tagId the identifier of the Tag object
     *  @return the number of unlinked pairs
     *  @throws DAOException if there is no appropriate pairs to unlink
     */
    int unLinkWithNewsOnTagDelete(Long tagId) throws DAOException;
}
