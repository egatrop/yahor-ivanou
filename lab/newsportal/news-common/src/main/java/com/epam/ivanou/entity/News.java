package com.epam.ivanou.entity;

import com.epam.ivanou.dao.Identified;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.util.*;

/**
 * The class represents the News entity and is mapped to the
 * NEWS table in the database.
 */
@Entity(name = "NEWS")
@Table(name = "NEWS")
@SequenceGenerator(name = "NEWS_SEQ", sequenceName = "NEWS_SEQ", allocationSize = 1)
public class News implements Identified {

    /*
     *The identifier of the news.
     *The field is mapped to the column NE_NEWS_ID_PK.
     */
    @Id
    @GeneratedValue(generator = "NEWS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "NE_NEWS_ID_PK", nullable = false)
    private Long id;

    /*
     *The title of the news.
     *The field is mapped to the column NE_TITLE.
     */
    @Column(name = "NE_TITLE", nullable = false)
    private String title;

    /*
     *The short content of the news.
     *The field is mapped to the column NE_SHORT_TEXT.
     */
    @Column(name = "NE_SHORT_TEXT", nullable = false)
    private String shortText;

    /*
     *The full content of the news.
     *The field is mapped to the column NE_FULL_TEXT.
     */
    @Column(name = "NE_FULL_TEXT", nullable = false)
    private String fullText;

    /*
     *The creation date.
     *The field is mapped to the column NE_CREATION_DATE.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NE_CREATION_DATE", nullable = false)
    private Date creationDate;

    /*
     *The modification date.
     *The field is mapped to the column NE_MODIFICATION_DATE.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NE_MODIFICATION_DATE", nullable = false)
    private Date modificationDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "NEWS_TAGS",
            joinColumns = {@JoinColumn(name = "NT_NEWS_ID_FK", referencedColumnName = "NE_NEWS_ID_PK")},
            inverseJoinColumns = {@JoinColumn(name = "NT_TAG_ID_FK", referencedColumnName = "TA_TAG_ID_PK")}
    )
    private Set<Tag> tags = new HashSet<>();

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tagList) {
        this.tags = tagList;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "NEWS_AUTHORS",
            joinColumns = {@JoinColumn(name = "NA_NEWS_ID_FK", referencedColumnName = "NE_NEWS_ID_PK")},
            inverseJoinColumns = {@JoinColumn(name = "NA_AUTHOR_ID_FK", referencedColumnName = "AU_AUTHOR_ID_PK")}
    )
    private Set<Author> authors = new HashSet<>();

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    @OneToMany(targetEntity = com.epam.ivanou.entity.Comment.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "CO_NEWS_ID_FK", referencedColumnName = "NE_NEWS_ID_PK")
    @OrderBy("creationDate DESC")
    private Set<Comment> comments = new TreeSet<>();

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    @Formula("(SELECT COUNT(*) FROM COMMENTS c WHERE c.CO_NEWS_ID_FK = NE_NEWS_ID_PK)")
    private int commentsCount;

    /**
     * Getter method for the property which is mapped to the column NE_NEWS_ID_PK.
     *
     * @return the value of the property
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter method for the property which is mapped to the column NE_NEWS_ID_PK.
     *
     * @param newsId the value to be set to the property
     */
    public void setId(Long newsId) {
        this.id = newsId;
    }

    /**
     * Getter method for the property which is mapped to the column NE_MODIFICATION_DATE.
     *
     * @return the value of the property
     */
    public Date getModificationDate() {
        return modificationDate;
    }

    /**
     * Setter method for the property which is mapped to the column NE_MODIFICATION_DATE.
     *
     * @param modificationDate the value to be set to the property
     */
    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    /**
     * Getter method for the property which is mapped to the column NE_TITLE.
     *
     * @return the value of the property
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter method for the property which is mapped to the column NE_TITLE.
     *
     * @param title the value to be set to the property
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter method for the property which is mapped to the column NE_SHORT_TEXT.
     *
     * @return the value of the property
     */
    public String getShortText() {
        return shortText;
    }

    /**
     * Setter method for the property which is mapped to the column NE_SHORT_TEXT.
     *
     * @param shortText the value to be set to the property
     */
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    /**
     * Getter method for the property which is mapped to the column NE_FULL_TEXT.
     *
     * @return the value of the property
     */
    public String getFullText() {
        return fullText;
    }

    /**
     * Setter method for the property which is mapped to the column NE_FULL_TEXT.
     *
     * @param fullText the value to be set to the property
     */
    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    /**
     * Getter method for the property which is mapped to the column NE_CREATION_DATE.
     *
     * @return the value of the property
     */
    public Date getCreationDate() {
        return creationDate;
    }/*public Timestamp getCreationDate() {
        return creationDate;
    }*/

    /**
     * Setter method for the property which is mapped to the column NE_CREATION_DATE.
     *
     * @param creationDate the value to be set to the property
     */
    /*public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }*/
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof News)) return false;
        News news = (News) o;
        return Objects.equals(getTitle(), news.getTitle()) &&
                Objects.equals(getShortText(), news.getShortText()) &&
                Objects.equals(getFullText(), news.getFullText()) &&
                Objects.equals(getCreationDate(), news.getCreationDate()) &&
                Objects.equals(getModificationDate(), news.getModificationDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getShortText(),
                getFullText(), getCreationDate(), getModificationDate());
    }

    /**
     * Returns a brief representation of the object.
     * The following may be regarded as typical:
     * "News{id=1, title='Belarus', shortText='Belarus. The election company.',
     * fullText='On the 11th of November...', creationDate=2015-08-30 18:53:29.477,
     * modificationDate=2015-08-30 18:53:29.477}"
     *
     * @return the string representation of the object
     */
    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", shortText='" + shortText + '\'' +
                ", fullText='" + fullText + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                '}';
    }

}
