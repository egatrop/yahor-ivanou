package com.epam.ivanou.exception;

/**
 * This exception is raised when there is an error
 * during executing methods of Services classes..
 */
public class ServiceException extends Exception {

    /**
     * Constructs a <code>ServiceException</code> with no detail message.
     */
    public ServiceException() {
    }

    /**
     * Constructs a <code>ServiceException</code> with the specified
     * detail message.
     *
     * @param message the detail message.
     */
    public ServiceException(String message) {
        super(message);
    }

    /**
     * Constructs a <code>ServiceException</code> with the specified
     * detail message and cause.
     *
     * @param message the detail message.
     * @param cause the cause.
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a <code>ServiceException</code> with the specified
     * cause.
     *
     * @param cause the cause.
     */
    public ServiceException(Throwable cause) {
        super(cause);
    }
}