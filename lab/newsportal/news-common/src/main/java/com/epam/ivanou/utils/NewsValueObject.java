package com.epam.ivanou.utils;


import com.epam.ivanou.entity.Author;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.entity.News;
import com.epam.ivanou.entity.Tag;

import java.util.*;

/**
 * Represents the bundle which consists of a News object, an Author object,
 * a list of Comment objects and a list of Tag objects with respectively
 * getters and setters.
 * <p>
 *    This utility class is used to pass the data between Service layer and other layers
 *    which may use the last one.
 * </p>
 */
public class NewsValueObject {

    private News news;
    private Author author;
    private List<Comment> comments = Collections.emptyList();
    private List<Tag> tags = Collections.emptyList();

    /**
     * Returns the <code>News</code> object.
     *
     * @return the News object.
     */
    public News getNews() {
        return news;
    }

    /**
     * Sets the inner <code>News</code>field with the passed object.
     *
     * @param news the News object
     */
    public void setNews(News news) {
        this.news = news;
    }

    /**
     * Returns the <code>Author</code> object.
     *
     * @return the Author object.
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * Sets the inner <code>Author</code> field with the passed object.
     *
     * @param author the Author object
     */
    public void setAuthor(Author author) {
        this.author = author;
    }

    /**
     * Returns the list of <code>Comment</code> objects.
     *
     * @return the list of Comment object
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * Sets the inner <tt>List&lt;Comment&gt;</tt> field with the passed list.
     *
     * @param comments the list of the Comment objects
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    /**
     * Adds the Comment object to the list of the Comment objects.
     *
     * @param comment the Comment object
     */
    public void addComment(Comment comment) {
        if (comments.isEmpty()) {
            comments = new ArrayList<>();
        }
        comments.add(comment);
    }

    /**
     * Returns the list of <code>Tag</code> objects.
     *
     * @return the list of Tag object
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the inner <tt>List&lt;Tag&gt;</tt> field with the passed list.
     *
     * @param tags the list of the Tag objects
     */
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    /**
     * Adds the Tag object to the list of the Tag objects.
     *
     * @param tag the Tag object
     */
    public void addTag(Tag tag) {
        if (tags.isEmpty()) {
            tags = new ArrayList<>();
        }
        tags.add(tag);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NewsValueObject)) return false;
        NewsValueObject that = (NewsValueObject) o;
        return Objects.equals(getNews(), that.getNews()) &&
                Objects.equals(getAuthor(), that.getAuthor()) &&
                Objects.equals(getComments(), that.getComments()) &&
                Objects.equals(getTags(), that.getTags());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNews(),
                getAuthor(), getComments(), getTags());
    }

    /**
     * Returns a brief representation of the object.
     * The following may be regarded as typical:
     * "NewsValueObject{news=News{id=1, title='Belarus', shortText='Belarus. The election company.',
     * fullText='On the 11th of November...', creationDate=2015-08-30 18:53:29.477,
     * modificationDate=2015-08-30 18:53:29.477},
     * author=Author{id=1, name='Agatha MacDonald', expiredDate=2017-08-13 09:58:44.316},
     * comments=[Comment{id=1, text='Bad news!', creationDate=2016-04-19 09:58:44.316, newsId=1},
     * Comment{id=2, text='Cooool!', creationDate=2016-04-19 09:58:44.316, newsId=1}],
     * tags=[Tag{id=1, name='WORK'}, Tag{id=2, name='HOME'}, Tag{id=3, name='TRAVELLING'}]}"
     *
     * @return the string representation of the object
     */
    @Override
    public String toString() {
        return "NewsValueObject{" +
                "news=" + news +
                ", author=" + author +
                ", comments=" + comments +
                ", tags=" + tags +
                '}';
    }
}
