package com.epam.ivanou.entity;

import com.epam.ivanou.dao.Identified;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

/**
 * The class represents the Comment entity and is mapped to the
 * COMMENTS table in the database.
 */
@Entity(name = "COMMENTS")
@Table(name = "COMMENTS")
@SequenceGenerator(name = "COMMENTS_SEQ", sequenceName = "COMMENTS_SEQ", allocationSize = 1)
public class Comment implements Identified {

    /*
     *The identifier of the comment.
     *The field is mapped to the column CO_COMMENT_ID_PK.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMENTS_SEQ")
    @Column(name = "CO_COMMENT_ID_PK", nullable = false)
    private Long id;

    /*
     *The text content of the comment.
     *The field is mapped to the column CO_COMMENT_TEXT.
     */
    @Column(name = "CO_COMMENT_TEXT", nullable = false)
    private String text;

    /*
     *The creation date of the comment.
     *The field is mapped to the column CO_CREATION_DATE.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CO_CREATION_DATE", nullable = false)
    private Date creationDate;

    /*
     *The identifier of the news the comment is associated with.
     *The field is mapped to the column CO_NEWS_ID_FK.
     */
    @Column(name = "CO_NEWS_ID_FK", nullable = false)
    private Long newsId;

    /**
     * Getter method for the property which is mapped to the column CO_COMMENT_ID_PK.
     *
     * @return the value of the property
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter method for the property which is mapped to the column CO_COMMENT_ID_PK.
     *
     * @param commentId the value to be set to the property
     */
    public void setId(Long commentId) {
        this.id = commentId;
    }

    /**
     * Getter method for the property which is mapped to the column CO_NEWS_ID_FK.
     *
     * @return the value of the property
     */
    public Long getNewsId() {
        return newsId;
    }

    /**
     * Setter method for the property which is mapped to the column CO_NEWS_ID_FK.
     *
     * @param id the value to be set to the property
     */
    public void setNewsId(Long id) {
        this.newsId = id;
    }

    /**
     * Getter method for the property which is mapped to the column CO_COMMENT_TEXT.
     *
     * @return the value of the property
     */
    public String getText() {
        return text;
    }

    /**
     * Setter method for the property which is mapped to the column CO_COMMENT_TEXT.
     *
     * @param text the value to be set to the property
     */
    public void setText(String text) {
        this.text = text;
    }


    /**
     * Getter method for the property which is mapped to the column CO_CREATION_DATE.
     *
     * @return the value of the property
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Setter method for the property which is mapped to the column CO_CREATION_DATE.
     *
     * @param creationDate the value to be set to the property
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", creationDate=" + creationDate +
                ", newsId=" + newsId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;
        Comment comment = (Comment) o;
        return  Objects.equals(getText(), comment.getText()) &&
                Objects.equals(getCreationDate(), comment.getCreationDate()) &&
                Objects.equals(getNewsId(), comment.getNewsId());
    }

    /**
     * Returns a brief representation of the object.
     * The following may be regarded as typical:
     * "Comment{id=1, text='GOOD NEWS', creationDate=2015-08-30 19:41:26.433, newsId=2}"
     *
     * @return the string representation of the object
     */
    @Override
    public int hashCode() {
        return Objects.hash(getText(),
                getCreationDate(), getNewsId());
    }
}
