package com.epam.ivanou.dao.impl.jdbc;

import com.epam.ivanou.dao.ITagDAO;
import com.epam.ivanou.entity.Tag;
import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.utils.DBUtils;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class TagJDBCDAO extends AbstractJDBCDAO<Tag> implements ITagDAO {

    private final static String TABLE_NAME_SQL = "TAGS";
    private final static String SEQ_NAME_SQL = "TAGS_SEQ";

    private final static String ID_SQL = "TA_TAG_ID_PK";
    private final static String NAME_SQL = "TA_TAG_NAME";

    private static final String CH_TABLE_NAME_SQL = "NEWS_TAGS";
    private static final String CH_NEWS_FK_SQL = "NT_NEWS_ID_FK";
    private static final String CH_TAG_FK_SQL = "NT_TAG_ID_FK";

    private final static String SELECT_QUERY_SQL = "SELECT " + ID_SQL
            + ", " + NAME_SQL
            + " FROM " + TABLE_NAME_SQL;

    private static final String CREATE_QUERY_SQL =
            "INSERT INTO " + TABLE_NAME_SQL
            + " (" + ID_SQL + ", " + NAME_SQL + ") "
            + "VALUES (" + SEQ_NAME_SQL + ".NEXTVAL , ?)";

    private static final String DELETE_QUERY_SQL =
            " DELETE FROM " + TABLE_NAME_SQL
            + " WHERE " + ID_SQL + " = ?";

    private static final String UPDATE_QUERY_SQL =
            "UPDATE " + TABLE_NAME_SQL
            + " SET " + NAME_SQL + " = ?  WHERE " + ID_SQL + " = ?";

    private static final String LINK_QUERY_SQL =
            "INSERT INTO NEWS_TAGS " +
            "(NT_NEWS_ID_FK, NT_TAG_ID_FK) VALUES(?,?)";

    private static final String UNLINK_QUERY_SQL =
            "DELETE FROM " +
                    CH_TABLE_NAME_SQL +
                    " WHERE " +
                    CH_NEWS_FK_SQL +
                    " = ?";

    private static final String SELECT_BY_NEWS_ID_SQL =
            "SELECT " + ID_SQL
            + ", " + NAME_SQL + " FROM " + TABLE_NAME_SQL + " INNER JOIN "
            + CH_TABLE_NAME_SQL
            + " ON " + ID_SQL + " = " + CH_TAG_FK_SQL + " WHERE " + CH_NEWS_FK_SQL + " = ?";

    private static final String UNLINK_BY_TAG_ID_QUERY_SQL = "DELETE FROM " +
            CH_TABLE_NAME_SQL +
            " WHERE " +
            CH_TAG_FK_SQL +
            " = ?";

    private DataSource dataSource;

    public TagJDBCDAO(DataSource dataSource) {
        super(dataSource);
        this.dataSource = dataSource;
    }

    @Override
    public void delete(Long tagId) throws DAOException {
        super.delete(tagId);
    }

    @Override
    public int linkWithNews(Long newsId, Long tagId) throws DAOException {
        int count = 0;
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(LINK_QUERY_SQL);
            statement.setLong(1, newsId);
            statement.setLong(2, tagId);
            count = statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return count;
    }

    @Override
    public int unLinkWithNewsOnNewsDelete(Long newsId) throws DAOException {
        int count = 0;
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(UNLINK_QUERY_SQL);
            statement.setLong(1, newsId);
            count = statement.executeUpdate();
            if (count == 0) {
                throw new DAOException("No one record was unlinked.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return count;
    }

    @Override
    public List<Tag> getAllByNewsId(Long newsId) throws DAOException {
        List<Tag> list;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(SELECT_BY_NEWS_ID_SQL);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            list = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(resultSet);
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return list.isEmpty() ? Collections.emptyList() : list;
    }

    @Override
    public int unLinkWithNewsOnTagDelete(Long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        int count = 0;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(UNLINK_BY_TAG_ID_QUERY_SQL);
            statement.setLong(1, tagId);
            count = statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return count;
    }

    @Override
    protected int[] getColumnIndexes() {
        return new int[]{1, 2};
    }

    @Override
    protected List<Tag> parseResultSet(ResultSet rs) throws DAOException {
        LinkedList<Tag> result = new LinkedList<>();

        try {
            while (rs.next()) {
                Tag tag = new Tag();
                tag.setId(rs.getLong(1));
                tag.setName(rs.getString(2));
                result.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement,
                                             Tag object) throws DAOException {
        try {
            statement.setString(1, object.getName());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement,
                                             Tag object) throws DAOException {
        try {
            statement.setString(1, object.getName());
            statement.setLong(2, object.getId());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected String getSelectQuery() {
        return SELECT_QUERY_SQL;
    }

    @Override
    protected String getCreateQuery() {
        return CREATE_QUERY_SQL;
    }

    @Override
    protected String getUpdateQuery() {
        return UPDATE_QUERY_SQL;
    }

    @Override
    protected String getDeleteQuery() {
        return DELETE_QUERY_SQL;
    }

    @Override
    protected String getIdString() {
        return ID_SQL;
    }
}
