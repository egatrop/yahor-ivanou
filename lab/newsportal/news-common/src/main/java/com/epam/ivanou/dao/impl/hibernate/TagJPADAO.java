package com.epam.ivanou.dao.impl.hibernate;

import com.epam.ivanou.dao.ITagDAO;
import com.epam.ivanou.entity.Tag;
import com.epam.ivanou.exception.DAOException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

public class TagJPADAO implements ITagDAO {

    public TagJPADAO() {
        super();
    }

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Tag create(Tag object) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(object);
        tx.commit();
        session.close();
        return object;
    }

    @Override
    public Tag getByPK(Long key) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Tag tag = (Tag) session.get(Tag.class, key);
        session.close();
        return tag;
    }

    @Override
    public void update(Tag object) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(object);
        tx.commit();
        session.close();
    }

    @Override
    public void delete(Long key) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Tag tag = (Tag) session.get(Tag.class, key);
        tag.setId(key);
        Query query = session.createSQLQuery("DELETE FROM NEWS_TAGS WHERE NT_TAG_ID_FK = ?");
        query.setLong(0, key).executeUpdate();
        session.delete(tag);
        tx.commit();
        session.close();
    }

    @Override
    @SuppressWarnings(value = "unchecked")
    public List<Tag> getAll() throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Tag> tags = session.createCriteria(Tag.class).list();
        tx.commit();
        session.close();
        return tags;
    }


    @Override
    public List<Tag> getAllByNewsId(Long newsId) throws DAOException {
        //NOP
        throw new UnsupportedOperationException();
    }

    @Override
    public int linkWithNews(Long newsId, Long tagId) throws DAOException {
        //NOP
        throw new UnsupportedOperationException();
    }

    @Override
    public int unLinkWithNewsOnNewsDelete(Long newsId) throws DAOException {
        //NOP
        throw new UnsupportedOperationException();
    }

    @Override
    public int unLinkWithNewsOnTagDelete(Long tagId) throws DAOException {
        //NOP
        throw new UnsupportedOperationException();
    }
}
