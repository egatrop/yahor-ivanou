package com.epam.ivanou.exception;

/**
 * An exception that provides information on a
 * persistent data access error or other errors.
 */
public class DAOException extends Exception {

    /**
     * Constructs a <code>DAOException</code> with no detail message.
     */
    public DAOException() {
    }

    /**
     * Constructs a <code>DAOException</code> with the specified
     * detail message.
     *
     * @param message the detail message.
     */
    public DAOException(String message) {
        super(message);
    }

    /**
     * Constructs a <code>DAOException</code> with the specified
     * detail message and cause.
     *
     * @param message the detail message.
     * @param cause the cause.
     */
    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a <code>DAOException</code> with the specified
     * cause.
     *
     * @param cause the cause.
     */
    public DAOException(Throwable cause) {
        super(cause);
    }
}