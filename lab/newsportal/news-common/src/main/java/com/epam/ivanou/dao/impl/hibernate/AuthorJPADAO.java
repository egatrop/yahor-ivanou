package com.epam.ivanou.dao.impl.hibernate;

import com.epam.ivanou.dao.IAuthorDAO;
import com.epam.ivanou.entity.Author;
import com.epam.ivanou.entity.News;
import com.epam.ivanou.exception.DAOException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class AuthorJPADAO implements IAuthorDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Author> getActualAuthors() throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Author> authors = session.createCriteria(Author.class)
                .add(Restrictions.isNull("expiredDate")).list();
        tx.commit();
        session.close();
        return authors;
    }

    @Override
    public Author create(Author object) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(object);
        tx.commit();
        session.close();
        return object;
    }

    @Override
    public Author getByPK(Long key) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Author tag = (Author) session.get(Author.class, key);
        session.close();
        return tag;
    }

    @Override
    public void update(Author object) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(object);
        tx.commit();
        session.close();
    }

    @Override
    public void delete(Long key) throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Author tag = (Author) session.get(Author.class, key);
        tag.setId(key);
        session.delete(tag);
        tx.commit();
        session.close();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Author> getAll() throws DAOException {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Author> authors = session.createCriteria(Author.class).list();
        tx.commit();
        session.close();
        return authors;
    }

    @Override
    public int linkWithNews(Long newsId, Long authorId) throws DAOException {
        //NOP
        throw new UnsupportedOperationException();
    }

    @Override
    public int unLinkWithNews(Long newsId) throws DAOException {
        //NOP
        throw new UnsupportedOperationException();
    }

    @Override
    public Author getByNewsId(Long newsId) throws DAOException {
        //NOP
        throw new UnsupportedOperationException();
    }
}
