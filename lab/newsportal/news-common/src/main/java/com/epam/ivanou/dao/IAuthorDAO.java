package com.epam.ivanou.dao;


import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.entity.Author;

import java.util.List;

/**
 * The interface to manage the <code>Author</code> entity.
 */
public interface IAuthorDAO extends IGenericDAO<Author> {

     /**
      * Links the <code>Author</code> object with the <code>News</code> object
      * appropriate to specified parameters.
      *
      *  @param newsId the identifier of the News object
      *  @param authorId the identifier of the Author object
      *  @return the number of linked pairs (either 1 if linking was successful
      *          or 0 if not)
      *  @throws DAOException
      */
     int linkWithNews(Long newsId, Long authorId) throws DAOException;

     /**
      * Unlinks the <code>Author</code> object with the <code>News</code> object
      * appropriate to specified parameters.
      *
      *  @param newsId the identifier of the News object
      *  @return the number of unlinked pairs
      *  @throws DAOException if there is no appropriate pairs to unlink.
      */
     int unLinkWithNews(Long newsId) throws DAOException;

     /**
      * Returns the list of actual(not expired) <code>Author</code> objects.
      *
      * @return the list of actual Author objects
      * @throws DAOException
      */
      List<Author> getActualAuthors() throws DAOException;

     /**
      * Returns the <code>Author</code> object associated
      * with the <code>News</code> object.
      *
      * @param newsId the identifier of the News object
      * @return the list of the Author objects appropriate to the
      *         <code>News</code> object
      * @throws DAOException
      */
     Author getByNewsId(Long newsId) throws DAOException;



}
