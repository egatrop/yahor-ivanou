package com.epam.ivanou.dao.impl.jdbc;

import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.dao.IGenericDAO;
import com.epam.ivanou.utils.DBUtils;
import com.epam.ivanou.dao.Identified;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * This class provides basic implementation of CRUD operations using JDBC.
 *
 * @param <T>  type of persistent object
 */
public abstract class AbstractJDBCDAO<T extends Identified> implements IGenericDAO<T> {

    private DataSource dataSource;

    public AbstractJDBCDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }



    @Override
    public T create(T object) throws DAOException {
        T persistInstance;
        String sql = getCreateQuery();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(sql, getColumnIndexes());
            prepareStatementForInsert(statement, object);

            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DAOException("On create modify more then 1 record: " + count);
            }

            resultSet = statement.getGeneratedKeys();
            List<T> list = parseResultSet(resultSet);

            if ((list == null) || (list.size() != 1)) {
                throw new DAOException("Exception on findByPK new persist data.");
            }

            persistInstance = list.iterator().next();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(resultSet);
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return persistInstance;
    }

    @Override
    public T getByPK(Long key) throws DAOException {
        List<T> list = null;
        String sql = getSelectQuery();
        sql += " WHERE " + getIdString() + " = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(sql);
            statement.setLong(1, key);
            resultSet = statement.executeQuery();
            list = parseResultSet(resultSet);
            if (list == null || list.size() == 0) {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(resultSet);
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }

        if (list.size() > 1) {
            throw new DAOException("Received more than one record.");
        }
        return list.iterator().next();
    }

    @Override
    public void update(T object) throws DAOException {
        String sql = getUpdateQuery();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(sql);
            prepareStatementForUpdate(statement, object);
            int count = statement.executeUpdate();
            if (count > 1) {
                throw new DAOException("On update modify more then 1 record: " + count);
            }
            if (count == 0) {
                throw new DAOException("No one record was modified. There is no "
                        + "such record with specified ID.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
    }

    @Override
    public void delete(Long key) throws DAOException {
        String sql = getDeleteQuery();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(sql);
            try {
                statement.setObject(1, key);
            } catch (SQLException e) {
                throw new DAOException(e);
            }
            int count = statement.executeUpdate();
            if (count > 1) {
                throw new DAOException("On delete modify more then 1 record: " + count);
            }
            if (count == 0) {
                throw new DAOException("No one record was deleted. There is no such "
                        + "record with specified ID.");
            }
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
    }

    @Override
    public List<T> getAll() throws DAOException {
        List<T> list;
        String sql = getSelectQuery();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            list = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(resultSet);
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return list.isEmpty() ? Collections.emptyList() : list;
    }


    /**
     * Returns indexes of columns from the entity table.
     */
    protected abstract int[] getColumnIndexes();

    /**
     * Parses the ResultSet and returns the list of objects corresponding to the ResultSet.
     */
    protected abstract List<T> parseResultSet(ResultSet rs) throws DAOException;

    /**
     * Sets the arguments of insert query in accordance to the values of object fields.
     */
    protected abstract void prepareStatementForInsert(PreparedStatement statement,
                                                      T object) throws DAOException;

    /**
     * Sets the arguments of update query in accordance to the values of object fields.
     */
    protected abstract void prepareStatementForUpdate(PreparedStatement statement,
                                                      T object) throws DAOException;

    /**
     * Returns sql query string to recieve all records.
     * <p>
     * SELECT * FROM [Table]
     *
     * @return a string, which presents an SQL statement
     */
    protected abstract String getSelectQuery();

    /**
     * Returns sql query string to insert new record in the database.
     * <p>
     * INSERT INTO [Table] ([column, column, ...]) VALUES (?, ?, ...);
     *
     * @return a string, which presents an SQL statement
     * that may contain one or more '?' IN parameter placeholders
     */
    protected abstract String getCreateQuery();

    /**
     * Returns sql query string to update the record.
     * <p>
     * UPDATE [Table] SET [column = ?, column = ?, ...] WHERE id = ?;
     *
     * @return a string, which presents an SQL statement
     * that may contain one or more '?' IN parameter placeholders
     */
    protected abstract String getUpdateQuery();

    /**
     * Returns sql query string to delete the record from the database.
     * <p>
     * DELETE FROM [Table] WHERE id= ?;
     *
     * @return a string, which presents an SQL statement
     * that may contain one or more '?' IN parameter placeholders
     */
    protected abstract String getDeleteQuery();

    /**
     * Returns String representation of column name of identification.
     *
     * @return String representation of column name of identification
     */
    protected abstract String getIdString();

}


