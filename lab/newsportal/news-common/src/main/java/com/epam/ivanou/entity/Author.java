package com.epam.ivanou.entity;


import com.epam.ivanou.dao.Identified;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;

/**
 * The class represents the Author entity and is mapped to the
 * AUTHORS table in the database.
 */
@Entity(name = "AUTHORS")
@Table(name = "AUTHORS")
@SequenceGenerator(name = "AUTHORS_SEQ", sequenceName = "AUTHORS_SEQ", allocationSize = 1)
public class Author implements Identified {

    /*
     *The identifier of the author.
     *The field is mapped to the column AU_AUTHOR_ID_PK.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTHORS_SEQ")
    @Column(name = "AU_AUTHOR_ID_PK", nullable = false)
    private Long id;

    /*
     *The expiration date. If the author is expired the value is not null.
     *The field is mapped to the column AU_EXPIRED.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "AU_EXPIRED", nullable = true)
    private Date expiredDate;

    /*
     *The name of the author.
     *The field is mapped to the column AU_AUTHOR_NAME.
     */
    @Column(name = "AU_AUTHOR_NAME", nullable = false)
    private String name;

    /**
     * Getter method for the property which is mapped to the column AU_AUTHOR_ID_PK.
     *
     * @return the value of the property
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter method for the property which is mapped to the column AU_AUTHOR_ID_PK.
     *
     * @param authorId the value to be set to the property
     */
    public void setId(Long authorId) {
        this.id = authorId;
    }

    /**
     * Getter method for the property which is mapped to the column AU_EXPIRED.
     *
     * @return the value of the property
     */
    public Date getExpiredDate() {
        return expiredDate;
    }

    /**
     * Setter method for the property which is mapped to the column AU_EXPIRED.
     *
     * @param expiredDate the value to be set to the property
     */
    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    /**
     * Getter method for the property which is mapped to the column AU_AUTHOR_NAME.
     *
     * @return the value of the property
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for the property which is mapped to the column AU_AUTHOR_NAME.
     *
     * @param name the value to be set to the property
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", expiredDate=" + expiredDate +
                '}';
    }

    /**
     * Returns a brief representation of the object.
     * The following may be regarded as typical:
     * "Author{id=1, name='Agatha MacDonald', expiredDate=2017-08-13 09:58:44.316}"
     *
     * @return the string representation of the object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Author)) return false;
        Author author = (Author) o;
        return  Objects.equals(getName(), author.getName()) &&
                Objects.equals(getExpiredDate(), author.getExpiredDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getExpiredDate());
    }
}
