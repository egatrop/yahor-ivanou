package com.epam.ivanou.dao.impl.jdbc;


import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.dao.INewsDAO;
import com.epam.ivanou.entity.News;
import com.epam.ivanou.utils.DBUtils;
import com.epam.ivanou.utils.SearchCriteria;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class NewsJDBCDAO extends AbstractJDBCDAO<News> implements INewsDAO {

    private final static String TABLE_NAME_SQL = "NEWS";
    private final static String SEQ_NAME_SQL = "NEWS_SEQ";

    private final static String ID_SQL = "NE_NEWS_ID_PK";
    private final static String TITLE_SQL = "NE_TITLE";
    private final static String SHORT_TEXT_SQL = "NE_SHORT_TEXT";
    private final static String FULL_TEXT_SQL = "NE_FULL_TEXT";
    private final static String CREATION_DATE_SQL = "NE_CREATION_DATE";
    private final static String MOD_DATE_SQL = "NE_MODIFICATION_DATE";

    private static final String TABLE_COMMENTS_SQL = "COMMENTS";
    private static final String TABLE_COMMENTS_NEWS_ID_FK_SQL = "CO_NEWS_ID_FK";
    private static final String TABLE_COMMENTS_COMMENT_ID_SQL = "CO_COMMENT_ID_PK";

    private static final String TABLE_NEWS_AUTHORS_SQL = "NEWS_AUTHORS";
    private static final String TABLE_NEWS_AUTHORS_NEWS_ID_FK_SQL = "NA_NEWS_ID_FK";
    private static final String TABLE_NEWS_AUTHORS_AUTHOR_ID_FK_SQL = "NA_AUTHOR_ID_FK";

    private static final String TABLE_NEWS_TAGS_SQL = "NEWS_TAGS";
    private static final String TABLE_NEWS_TAGS_NEWS_ID_FK_SQL = "NT_NEWS_ID_FK";
    private static final String TABLE_NEWS_TAGS_TAG_ID_FK_SQL = "NT_TAG_ID_FK";

    private final static String SELECT_ALL_QUERY_SQL =
            "SELECT " + ID_SQL + ", " + TITLE_SQL + ", " + SHORT_TEXT_SQL
                    + ", " + FULL_TEXT_SQL + ", " + CREATION_DATE_SQL
                    + ", " + MOD_DATE_SQL + ", COUNT(DISTINCT " +
                    TABLE_COMMENTS_COMMENT_ID_SQL +
                    ") CN" + " FROM " + TABLE_NAME_SQL
                    + " LEFT JOIN " + TABLE_COMMENTS_SQL + " C ON " + ID_SQL + " = "
                    + TABLE_COMMENTS_NEWS_ID_FK_SQL
                    + " LEFT JOIN " + TABLE_NEWS_AUTHORS_SQL + " NA ON "
                    + TABLE_NEWS_AUTHORS_NEWS_ID_FK_SQL + " = " + ID_SQL
                    + " LEFT JOIN " + TABLE_NEWS_TAGS_SQL + " NT ON " + ID_SQL
                    + " = " + TABLE_NEWS_TAGS_NEWS_ID_FK_SQL
                    + " GROUP BY " + ID_SQL + ", " + TITLE_SQL + ", "
                    + SHORT_TEXT_SQL + ", " + FULL_TEXT_SQL + ", " + CREATION_DATE_SQL + ", "
                    + MOD_DATE_SQL + " ORDER BY CN DESC, " + MOD_DATE_SQL + " DESC";

    private final static String SELECT_QUERY_SQL =
            "SELECT " + ID_SQL + ", " + TITLE_SQL + ", " + SHORT_TEXT_SQL
                    + ", " + FULL_TEXT_SQL + ", "
                    + CREATION_DATE_SQL + ", " + MOD_DATE_SQL + " FROM " + TABLE_NAME_SQL;

    private static final String CREATE_QUERY_SQL = "INSERT INTO "
            + TABLE_NAME_SQL + " (" + ID_SQL + ", " + TITLE_SQL + ", " + SHORT_TEXT_SQL
            + ", " + FULL_TEXT_SQL
            + ", " + CREATION_DATE_SQL + ", " + MOD_DATE_SQL
            + ") VALUES ( " + SEQ_NAME_SQL + ".NEXTVAL , ?, ?, ?, ?, ?)";

    private static final String DELETE_QUERY_SQL = "DELETE FROM " + TABLE_NAME_SQL
            + " WHERE " + ID_SQL + " = ?";

    private static final String UPDATE_QUERY_SQL =  "UPDATE " + TABLE_NAME_SQL
            + " SET " + TITLE_SQL + " = ?, " + SHORT_TEXT_SQL + " = ?, " + FULL_TEXT_SQL
            + " = ?, " + CREATION_DATE_SQL + " = ?, " + MOD_DATE_SQL + " = ?  WHERE "
            + ID_SQL + " = ?";;

    private static final String COUNT_ALL_QUERY_SQL = "SELECT COUNT(*) FROM " + TABLE_NAME_SQL;


    private DataSource dataSource;

    public NewsJDBCDAO(DataSource dataSource) {
        super(dataSource);
        this.dataSource = dataSource;
    }

    @Override
    public void delete(Long newsId) throws DAOException {
        super.delete(newsId);
    }

    @Override
    public int countAll() throws DAOException {
        int count = 0;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(COUNT_ALL_QUERY_SQL);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                count = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(resultSet);
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return count;
    }

    @Override
    public List<News> getAll() throws DAOException {
        List<News> list;
        String sql = SELECT_ALL_QUERY_SQL;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            list = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(resultSet);
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return list.isEmpty() ? Collections.emptyList() : list;
    }

    @Override
    public List<News> search(SearchCriteria criteria) throws DAOException {
        List<News> list;
        String sql = QueryBuilder.buildQueryString(criteria);
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            list = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(resultSet);
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return list.isEmpty() ? Collections.emptyList() : list;
    }

    private static class QueryBuilder {
        private static final String START_QUERY_SQL = "SELECT " + ID_SQL
                + ", " + TITLE_SQL + ", " + SHORT_TEXT_SQL + ", " + FULL_TEXT_SQL
                + ", " + CREATION_DATE_SQL + ", " + MOD_DATE_SQL + ", COUNT(DISTINCT " +
                TABLE_COMMENTS_COMMENT_ID_SQL +
                ") CN" + " FROM " + TABLE_NAME_SQL
                + " LEFT JOIN " + TABLE_COMMENTS_SQL + " C ON " + ID_SQL + " = "
                + TABLE_COMMENTS_NEWS_ID_FK_SQL
                + " LEFT JOIN " + TABLE_NEWS_AUTHORS_SQL + " NA ON "
                + TABLE_NEWS_AUTHORS_NEWS_ID_FK_SQL + " = " + ID_SQL
                + " LEFT JOIN " + TABLE_NEWS_TAGS_SQL + " NT ON " + ID_SQL + " = "
                + TABLE_NEWS_TAGS_NEWS_ID_FK_SQL;

        private static final String END_QUERY_SQL = " GROUP BY " + ID_SQL + ", "
                + TITLE_SQL + ", " + SHORT_TEXT_SQL + ", " + FULL_TEXT_SQL
                + ", " + CREATION_DATE_SQL + ", " + MOD_DATE_SQL + " ORDER BY CN DESC, "
                + MOD_DATE_SQL + " DESC";

        public static String buildQueryString(SearchCriteria criteria) {

            if(criteria == null || (criteria.getAuthorId() == null
                    && criteria.getTagsIdSet().isEmpty())) return SELECT_ALL_QUERY_SQL;

            StringBuilder builder = new StringBuilder(" WHERE ");
            String authorID = "";

            if(criteria.getAuthorId() != null) {
                authorID = String.valueOf(criteria.getAuthorId());
                builder.append(TABLE_NEWS_AUTHORS_AUTHOR_ID_FK_SQL)
                        .append(" = ")
                        .append(authorID);
            }

            if(!criteria.getTagsIdSet().isEmpty()) {
                if (!authorID.isEmpty()) builder.append(" AND ");
                builder.append(TABLE_NEWS_TAGS_TAG_ID_FK_SQL)
                        .append(" IN (");
                Object[] tempArr = criteria.getTagsIdSet().toArray();
                for (int i = 0; i < tempArr.length; i++) {
                    builder.append(String.valueOf(tempArr[i]));
                    if(i != (tempArr.length - 1)) builder.append(", ");
                }
                builder.append(")");
            }
            String whereClause = builder.toString();
            return START_QUERY_SQL + whereClause + END_QUERY_SQL;
        }
    }

    @Override
    protected int[] getColumnIndexes() {
        return new int[]{1, 2, 3, 4, 5, 6};
    }

    @Override
    protected List<News> parseResultSet(ResultSet rs) throws DAOException {
        LinkedList<News> result = new LinkedList<>();
        try {
            while (rs.next()) {
                News news = new News();
                news.setId(rs.getLong(1));
                news.setTitle(rs.getString(2));
                news.setShortText(rs.getString(3));
                news.setFullText(rs.getString(4));
                news.setCreationDate(rs.getTimestamp(5));
                news.setModificationDate(rs.getTimestamp(6));
                result.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement,
                                             News object) throws DAOException {
        try {
            statement.setString(1, object.getTitle());
            statement.setString(2, object.getShortText());
            statement.setString(3, object.getFullText());
            statement.setTimestamp(4, (Timestamp) object.getCreationDate());
            statement.setTimestamp(5, (Timestamp) object.getModificationDate());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement,
                                             News object) throws DAOException {
        try {
            statement.setString(1, object.getTitle());
            statement.setString(2, object.getShortText());
            statement.setString(3, object.getFullText());
            statement.setTimestamp(4, (Timestamp) object.getCreationDate());
            statement.setTimestamp(5, (Timestamp) object.getModificationDate());
            statement.setLong(6, object.getId());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected String getSelectQuery() {
        return SELECT_QUERY_SQL;
    }

    @Override
    protected String getCreateQuery() {
        return CREATE_QUERY_SQL;
    }

    @Override
    protected String getUpdateQuery() {
        return UPDATE_QUERY_SQL;
    }

    @Override
    protected String getDeleteQuery() {
        return DELETE_QUERY_SQL;
    }

    @Override
    protected String getIdString() {
        return ID_SQL;
    }
}
