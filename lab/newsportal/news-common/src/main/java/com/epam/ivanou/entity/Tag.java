package com.epam.ivanou.entity;

import com.epam.ivanou.dao.Identified;

import javax.persistence.*;
import java.util.*;

/**
 * The class represents the Tag entity and is mapped to the
 * TAGS table in the database.
 */
@Entity(name = "TAGS")
@Table(name = "TAGS")
@SequenceGenerator(name = "TAGS_SEQ", sequenceName = "TAGS_SEQ", allocationSize = 1)
public class Tag implements Identified {

    /*
     *The identifier of the tag.
     *The field is mapped to the column TA_TAG_ID_PK.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAGS_SEQ")
    @Column(name = "TA_TAG_ID_PK", nullable = false)
    private Long id;

    /*
     *The name of the tag.
     *The field is mapped to the column TA_TAG_NAME.
     */
    @Column(name = "TA_TAG_NAME", nullable = false)
    private String name;

    /**
     * Getter method for the property which is mapped to the column TA_TAG_ID_PK.
     *
     * @return the value of the property
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter method for the property which is mapped to the column TA_TAG_ID_PK.
     *
     * @param tagId the value to be set to the property
     */
    public void setId(Long tagId) {
        this.id = tagId;
    }

    /**
     * Getter method for the property which is mapped to the column TA_TAG_NAME.
     *
     * @return the value of the property
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for the property which is mapped to the column TA_TAG_NAME.
     *
     * @param name the value to be set to the property
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;
        Tag tag = (Tag) o;
        return Objects.equals(getName(), tag.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    /**
     * Returns a brief representation of the object.
     * The following may be regarded as typical:
     * "Tag{id=1, name='Belarus'}"
     *
     * @return the string representation of the object
     */
    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
