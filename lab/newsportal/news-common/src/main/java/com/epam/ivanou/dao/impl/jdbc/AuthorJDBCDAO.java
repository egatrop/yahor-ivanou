package com.epam.ivanou.dao.impl.jdbc;

import com.epam.ivanou.dao.IAuthorDAO;
import com.epam.ivanou.entity.Author;
import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.utils.DBUtils;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class AuthorJDBCDAO extends AbstractJDBCDAO<Author> implements IAuthorDAO {

    private final static String TABLE_NAME_SQL = "AUTHORS";
    private final static String SEQ_NAME_SQL = "AUTHORS_SEQ";

    private static final String CH_TABLE_NAME_SQL = "NEWS_AUTHORS";
    private static final String CH_NEWS_FK_SQL = "NA_NEWS_ID_FK";
    private static final String CH_AUTH_FK_SQL = "NA_AUTHOR_ID_FK";

    private final static String ID_SQL = "AU_AUTHOR_ID_PK";
    private final static String NAME_SQL = "AU_AUTHOR_NAME";
    private final static String EXPIRED_DATE_SQL = "AU_EXPIRED";

    private final static String SELECT_QUERY_SQL =
            "SELECT " + ID_SQL + ", " + NAME_SQL
            + ", " + EXPIRED_DATE_SQL + " FROM " + TABLE_NAME_SQL;

    private static final String CREATE_QUERY_SQL =
            "INSERT INTO " + TABLE_NAME_SQL
            + " (" + ID_SQL + ", " + NAME_SQL + ", " + EXPIRED_DATE_SQL + ") "
            + "VALUES (" + SEQ_NAME_SQL + ".NEXTVAL , ?, ?)";

    private static final String DELETE_QUERY_SQL =
            " DELETE FROM " + TABLE_NAME_SQL
            + " WHERE " + ID_SQL + " = ?";

    private static final String UPDATE_QUERY_SQL =
            "UPDATE " + TABLE_NAME_SQL + " SET " + NAME_SQL + " = ? ,"
            + EXPIRED_DATE_SQL + " = ? WHERE " + ID_SQL + " = ?";

    public static final String LINK_QUERY_SQL =
            "INSERT INTO " + CH_TABLE_NAME_SQL
            + " (" + CH_NEWS_FK_SQL + ", " + CH_AUTH_FK_SQL + ") VALUES(?,?)";

    public static final String UNLINK_QUERY_SQL = "DELETE FROM " + CH_TABLE_NAME_SQL
            + " WHERE " + CH_NEWS_FK_SQL + " = ?";

    private static final String SELECT_ACTUAL_SQL = "SELECT " + ID_SQL + ", " + NAME_SQL
            + ", " + EXPIRED_DATE_SQL + " FROM " + TABLE_NAME_SQL +
            " WHERE " + EXPIRED_DATE_SQL + " IS NULL";

    private static final String SELECT_BY_NEWS_ID_SQL = "SELECT " + ID_SQL + ", " + NAME_SQL
            + ", " + EXPIRED_DATE_SQL + " FROM " + TABLE_NAME_SQL + " INNER JOIN " + CH_TABLE_NAME_SQL
            + " ON " + ID_SQL + " = " + CH_AUTH_FK_SQL + " WHERE " + CH_NEWS_FK_SQL + " = ?";

    private DataSource dataSource;

    public AuthorJDBCDAO(DataSource dataSource) {
        super(dataSource);
        this.dataSource = dataSource;
    }

    @Override
    public void delete(Long authorId) throws DAOException {
        super.delete(authorId);
    }

    @Override
    public int linkWithNews(Long newsId, Long authorId) throws DAOException {
        int count = 0;
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(LINK_QUERY_SQL);
            statement.setLong(1, newsId);
            statement.setLong(2, authorId);
            count = statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return count;

    }


    @Override
    public int unLinkWithNews(Long newsId) throws DAOException {
        int count = 0;
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(UNLINK_QUERY_SQL);
            statement.setLong(1, newsId);
            count = statement.executeUpdate();
            if (count == 0) {
                throw new DAOException("No one record was unlinked.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }

        return count;
    }

    @Override
    public List<Author> getActualAuthors() throws DAOException {
        List<Author> list;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(SELECT_ACTUAL_SQL);
            resultSet = statement.executeQuery();
            list = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(resultSet);
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return list.isEmpty() ? Collections.emptyList() : list;
    }

    @Override
    public Author getByNewsId(Long newsId) throws DAOException {
        Author author;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            statement = connection.prepareStatement(SELECT_BY_NEWS_ID_SQL);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            author = parseResultSet(resultSet).get(0);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtils.close(resultSet);
            DBUtils.close(statement);
            DBUtils.close(dataSource, connection);
        }
        return author != null ? author : null;
    }

    @Override
    protected List<Author> parseResultSet(ResultSet rs) throws DAOException {
        LinkedList<Author> result = new LinkedList<>();

        try {
            while (rs.next()) {
                Author author = new Author();
                author.setId(rs.getLong(1));
                author.setName(rs.getString(2));
                author.setExpiredDate(rs.getTimestamp(3));
                result.add(author);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement,
                                             Author object) throws DAOException {
        try {
            statement.setString(1, object.getName());
            statement.setTimestamp(2, (Timestamp) object.getExpiredDate());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement,
                                             Author object) throws DAOException {
        try {
            statement.setString(1, object.getName());
            statement.setTimestamp(2, (Timestamp) object.getExpiredDate());
            statement.setLong(3, object.getId());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    protected String getSelectQuery() {
        return SELECT_QUERY_SQL;
    }

    @Override
    protected String getCreateQuery() {
        return CREATE_QUERY_SQL;
    }

    @Override
    protected String getUpdateQuery() {
        return UPDATE_QUERY_SQL;
    }

    @Override
    protected String getDeleteQuery() {
        return DELETE_QUERY_SQL;
    }

    @Override
    protected String getIdString() {
        return ID_SQL;
    }

    @Override
    protected int[] getColumnIndexes() {
        return new int[]{1, 2, 3};
    }

}
