package com.epam.ivanou.service.impl.jdbc;

import com.epam.ivanou.dao.IAuthorDAO;
import com.epam.ivanou.entity.Author;
import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.service.IAuthorService;
import com.epam.ivanou.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

public class AuthorJDBCService implements IAuthorService {

    private static final Logger logger = Logger.getLogger(AuthorJDBCService.class);

    private IAuthorDAO authorDAO;

    public AuthorJDBCService(IAuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    @Override
    public Author get(Long authorId) throws ServiceException {
        Author author;

        try {
            author = authorDAO.getByPK(authorId);
            if (author == null) throw new ServiceException("No such author in the database");
        } catch (DAOException e) {
            logger.error("Exception when trying to retrieve the author with id = "
                    + authorId + ".");
            throw new ServiceException(e);
        }
        return author;
    }

    @Override
    public Long add(Author author) throws ServiceException {
        Long id;

        try {
            id = authorDAO.create(author).getId();
            logger.info("The author with id = " + id + " was added.");
        } catch (DAOException e) {
            logger.error("Exception when trying to add the author to DB.", e);
            throw new ServiceException(e);
        }
        return id;
    }

    @Override
    public void update(Author author) throws ServiceException {
        try {
            authorDAO.update(author);
            logger.info("The author with id = " + author.getId()
                    + " was updated. Current parameters : "
                    + "name = " + author.getName() + ", expired date = "
                    + author.getExpiredDate() + ".");
        } catch (DAOException e) {
            logger.error("Exception when trying to update the author with id = "
                    + author.getId() + ".");
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Author> getAll() throws ServiceException {
        List<Author> list;

        try {
            list = authorDAO.getAll();
        } catch (DAOException e) {
            logger.error("Exception when trying to retrieve the list of authors.", e);
            throw new ServiceException(e);
        }
        return list;
    }

    @Override
    public Author getByNewsId(Long newsId) throws ServiceException {
        Author author;

        try {
            author = authorDAO.getByNewsId(newsId);
        } catch (DAOException e) {
            logger.error("Exception when trying to get the author of the news. News' id = "
                    + newsId + ".", e);
            throw new ServiceException(e);
        }
        return author;
    }

    @Override
    public int linkWithNews(Long newsId, Long authorId) throws ServiceException {
        int count;

        try {
            count = authorDAO.linkWithNews(newsId, authorId);
            logger.info("The author(id = " + authorId + ") was added to the news(id = "
                    + newsId + ").");
        } catch (DAOException e) {
            logger.error("Exception when trying to link the author " +
                    "with the news. Author's id = "
                    + authorId + ", news' id = " + newsId + ".", e);
            throw new ServiceException(e);
        }
        return count;
    }

    @Override
    public int unlinkWithNews(Long newsId) throws ServiceException {
        int count;

        try {
            count = authorDAO.unLinkWithNews(newsId);
        } catch (DAOException e) {
            logger.error("Exception when trying to unlink the news with author. News' id = "
                    + newsId + ".", e);
            throw new ServiceException(e);
        }
        return count;
    }
}
