package com.epam.ivanou.dao;


import com.epam.ivanou.entity.News;
import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.utils.SearchCriteria;

import java.util.List;

/**
 * The interface to manage the <code>News</code> entity.
 *
 * @see SearchCriteria
 */
public interface INewsDAO extends IGenericDAO<News> {

     /**
      * Returns the total number of all the <code>News</code> items within
      * the data source.
      *
      * @return the total number of all <code>News</code> items
      * @throws DAOException
      */
     int countAll() throws DAOException;

     /**
      * Returns the list of the <code>News</code> objects appropriate to the search parameters
      * passed through the <code>SearchCriteria</code> object, ordered by
      * the most commented <code>News</code> item and modification date.
      *
      * @param criteria the SearchCriteria object with defined parameters
      * @return the list of News objects appropriate to the search parameters
      * @throws DAOException
      */
     List<News> search(SearchCriteria criteria) throws DAOException;

}
