package com.epam.ivanou.service.impl.jdbc;

import com.epam.ivanou.dao.INewsDAO;
import com.epam.ivanou.entity.News;
import com.epam.ivanou.service.INewsService;
import com.epam.ivanou.service.impl.jdbc.NewsJDBCService;
import com.epam.ivanou.utils.SearchCriteria;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.unitils.UnitilsJUnit4;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class NewsServiceTest extends UnitilsJUnit4 {

    @Mock
    private INewsDAO mockedNewsDAO;

    private INewsService newsService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        newsService = new NewsJDBCService(mockedNewsDAO);
        assertNotNull(mockedNewsDAO);
        assertNotNull(newsService);
    }

    @Test
    public void testAdd() throws Exception {
        Long expected = 1L;
        News news = new News();
        news.setId(1l);
        when(mockedNewsDAO.create(news)).thenReturn(news);
        Long actual = newsService.add(news);
        verify(mockedNewsDAO, times(1)).create(news);
        assertEquals(expected, actual);
    }

    @Test
    public void testUpdate() throws Exception {
        News news = new News();
        newsService.update(news);
        verify(mockedNewsDAO).update(news);
    }

    @Test
    public void testDelete() throws Exception {
        newsService.delete(1L);
        verify(mockedNewsDAO, times(1)).delete(1L);
    }

    @Test
    public void testGet() throws Exception {
        Long expected = 1L;
        News news = new News();
        news.setId(1L);
        when(mockedNewsDAO.getByPK(1L)).thenReturn(news);
        Long actual = newsService.get(expected).getId();
        verify(mockedNewsDAO, times(1)).getByPK(1L);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetAll() throws Exception {
        ArrayList<News> news = new ArrayList<>();
        news.add(new News());
        news.add(new News());
        news.add(new News());
        when(mockedNewsDAO.getAll()).thenReturn(news);
        int expected = 3;
        int actual = newsService.getAll().size();
        verify(mockedNewsDAO, times(1)).getAll();
        assertEquals(expected, actual);
    }

    @Test
    public void testSearch() throws Exception {
        ArrayList<News> news = new ArrayList<>();
        SearchCriteria criteria = new SearchCriteria();
        criteria.addTagId(1L);
        criteria.addTagId(2L);
        criteria.setAuthorId(2l);
        News expected = new News();
        news.add(expected);
        expected.setId(10L);
        when(mockedNewsDAO.search(criteria)).thenReturn(news);
        News actual = newsService.search(criteria).get(0);
        verify(mockedNewsDAO, times(1)).search(criteria);
        assertEquals(expected.getId(), actual.getId());
    }

    @Test
    public void testCountAll() throws Exception {
        int expected = 3;
        when(mockedNewsDAO.countAll()).thenReturn(expected);
        int actual = newsService.countAll();
        verify(mockedNewsDAO, times(1)).countAll();
        assertEquals(expected, actual);
    }
}