package com.epam.ivanou.dao.impl.jdbc;

import com.epam.ivanou.entity.News;
import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.utils.SearchCriteria;
import com.epam.ivanou.dao.INewsDAO;
import org.junit.BeforeClass;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

@SpringApplicationContext({"spring-config.xml"})
@Transactional(TransactionMode.ROLLBACK)
@DataSet(value = "test/dao/test-dataset.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class NewsDAOTest extends UnitilsJUnit4 {

    @SpringBean("newsDAO")
    private INewsDAO newsDAO;

    @BeforeClass
    public static void setSystemProperty() {
        Properties properties = System.getProperties();
        properties.setProperty("spring.profiles.active", "jdbc");
    }

    @Test
    public void testCreate() throws Exception {
        News expected = new News();
        expected.setId(1L);
        expected.setTitle("Belarus. The Election race.");
        expected.setShortText("All Europe is looking after political ...");
        expected.setFullText("All Europe is looking after political activity in Belarus.");
        expected.setCreationDate(java.sql.Timestamp.valueOf("2016-04-19 09:58:44"));
        expected.setModificationDate(java.sql.Timestamp.valueOf("2015-08-30 18:53:34.0"));
        News actual = newsDAO.create(expected);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetByPK() throws Exception {
        News expected = new News();
        expected.setId(-1L);
        expected.setTitle("Bangkok Bombing");
        expected.setShortText("BANGKOK Thai police said ...");
        expected.setFullText("The unnamed foreigner was arrested Saturday at an apartment" +
                " on the outskirts of");
        expected.setCreationDate(java.sql.Timestamp.valueOf("2015-08-30 18:53:29.477"));
        expected.setModificationDate(java.sql.Timestamp.valueOf("2015-08-30 18:53:34.0"));
        News actual = newsDAO.getByPK(-1L);
        assertEquals(expected, actual);
    }

    @Test(expected = DAOException.class)
    public void testDelete() throws Exception {
        Long expected = -6L;
        newsDAO.delete(expected);
        assertNull(newsDAO.getByPK(-6L));
        newsDAO.delete(expected);
    }

    @Test
    public void testGetAll() throws Exception {
        long[] expecteds = {-3L, -1L, -2L, -4, -6, -5}; //right order of the ids of the sorted news
        List<News> resultList = newsDAO.getAll();
        long[] actuals = new long[resultList.size()];
        for(int i = 0; i < actuals.length;  i++) {
            actuals[i] = resultList.get(i).getId();
        }
        assertArrayEquals(expecteds, actuals);
    }

    @Test(expected = DAOException.class)
    public void testUpdate() throws Exception {
        News expected = new News();
        expected.setId(-1L);
        expected.setTitle("Bangkok Bombing");
        expected.setShortText("BANGKOK");
        expected.setFullText("The unnamed foreigner");
        expected.setCreationDate(java.sql.Timestamp.valueOf("2015-01-30 18:53:29.477"));
        expected.setModificationDate(java.sql.Timestamp.valueOf("2013-08-30 18:53:34.0"));
        try {
            newsDAO.update(expected);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        News actual = newsDAO.getByPK(expected.getId());
        assertEquals(expected, actual);
        expected.setId(10000L);
        newsDAO.update(expected);
    }

    @Test
    public void testSearch() throws Exception {
        long[] expecteds = {-3L, -2L}; //right order of the ids of the sorted news
        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(-2L);
        sc.addTagId(-1L);
        sc.addTagId(-3L);
        sc.addTagId(-4L);
        sc.addTagId(-5L);
        sc.addTagId(-6L);
        sc.addTagId(-7L);
        List<News> resultList = newsDAO.search(sc);
        long[] actuals = new long[resultList.size()];
        for(int i = 0; i < actuals.length;  i++) {
            actuals[i] = resultList.get(i).getId();
        }
        assertArrayEquals(expecteds, actuals);
        sc = new SearchCriteria();
        sc.setAuthorId(-19L);
        assertTrue(newsDAO.search(sc).isEmpty());
        sc = new SearchCriteria();
        sc.addTagId(-3L);
        sc.addTagId(-4L);
        sc.addTagId(-5L);
        sc.addTagId(-6L);
        expecteds = new long[]{-3L, -2L, -5L};
        resultList = newsDAO.search(sc);
        actuals = new long[resultList.size()];
        for(int i = 0; i < actuals.length;  i++) {
            actuals[i] = resultList.get(i).getId();
        }
        assertArrayEquals(expecteds, actuals);

        sc = new SearchCriteria();
        expecteds = new long[]{-3L, -1L, -2L, -4L, -6L, -5L};
        resultList = newsDAO.search(sc);
        actuals = new long[resultList.size()];
        for(int i = 0; i < actuals.length;  i++) {
            actuals[i] = resultList.get(i).getId();
        }
        assertArrayEquals(expecteds, actuals);
        assertArrayEquals(expecteds, actuals);
    }

    @Test
    public void testCountAll() throws Exception {
        int expected = 6;
        int actual = newsDAO.countAll();
        assertEquals(expected, actual);
    }
}