package com.epam.ivanou.service.impl.jdbc;

import com.epam.ivanou.dao.ITagDAO;
import com.epam.ivanou.entity.Tag;
import com.epam.ivanou.service.ITagService;
import com.epam.ivanou.service.impl.jdbc.TagJDBCService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.unitils.UnitilsJUnit4;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class TagServiceTest extends UnitilsJUnit4 {

    @Mock
    private ITagDAO mockedTagDAO;

    private ITagService tagService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        tagService = new TagJDBCService(mockedTagDAO);
        assertNotNull(mockedTagDAO);
        assertNotNull(tagService);
    }

    @Test
    public void testAdd() throws Exception {
        Long expected = 1L;
        Tag tag = new Tag();
        tag.setId(1l);
        when(mockedTagDAO.create(tag)).thenReturn(tag);
        Long actual = tagService.add(tag);
        verify(mockedTagDAO, times(1)).create(tag);
        assertEquals(expected, actual);
    }

    @Test
    public void testUpdate() throws Exception {
        Tag tag = new Tag();
        tag.setName("Yahor");
        tagService.update(tag);
        verify(mockedTagDAO).update(tag);
    }

    @Test
    public void testDelete() throws Exception {
        tagService.delete(1L);
        verify(mockedTagDAO, times(1)).delete(1L);
        verify(mockedTagDAO, times(1)).unLinkWithNewsOnTagDelete(1L);
    }

    @Test
    public void testGetAll() throws Exception {
        ArrayList<Tag> tags = new ArrayList<>();
        tags.add(new Tag());
        tags.add(new Tag());
        tags.add(new Tag());
        when(mockedTagDAO.getAll()).thenReturn(tags);
        int expected = 3;
        int actual = tagService.getAll().size();
        verify(mockedTagDAO, times(1)).getAll();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetAllByNewsId() throws Exception {
        ArrayList<Tag> tags = new ArrayList<>();
        tags.add(new Tag());
        tags.add(new Tag());
        tags.add(new Tag());
        when(mockedTagDAO.getAllByNewsId(1L)).thenReturn(tags);
        int expected = 3;
        int actual = tagService.getAllByNewsId(1L).size();
        verify(mockedTagDAO, times(1)).getAllByNewsId(1L);
        assertEquals(expected, actual);
    }

    @Test
    public void testLinkWithNews() throws Exception {
        when(mockedTagDAO.linkWithNews(1L, 2L)).thenReturn(1);
        int expected = 1;
        int actual = tagService.linkWithNews(1l, 2l);
        verify(mockedTagDAO, times(1)).linkWithNews(1L, 2L);
        assertEquals(expected, actual);
    }

    @Test
    public void testUnlinkWithNews() throws Exception {
        when(mockedTagDAO.unLinkWithNewsOnNewsDelete(1L)).thenReturn(5);
        int expected = 5;
        int actual = tagService.unlinkWithNews(1l);
        verify(mockedTagDAO, times(1)).unLinkWithNewsOnNewsDelete(1L);
        assertEquals(expected, actual);
    }
}