package com.epam.ivanou.dao.impl.jdbc;

import com.epam.ivanou.dao.IAuthorDAO;
import com.epam.ivanou.entity.Author;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

@SpringApplicationContext({"spring-config.xml"})
@Transactional(TransactionMode.ROLLBACK)
@DataSet(value = "test/dao/test-dataset.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class AuthorDAOTest extends UnitilsJUnit4 {

    @SpringBean("authorDAO")
    private IAuthorDAO authorDAO;

    @BeforeClass
    public static void setSystemProperty() {
        Properties properties = System.getProperties();
        properties.setProperty("spring.profiles.active", "jdbc");
    }

    @Test
    public void testCreate() throws Exception {
        Author expected = new Author();
        expected.setId(1L);
        expected.setName("John Doe");
        expected.setExpiredDate(Timestamp.valueOf(LocalDateTime.now()));
        Author actual = authorDAO.create(expected);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getExpiredDate(), actual.getExpiredDate());
        assertEquals(expected.getName(), actual.getName());
    }

    @Test
    public void testGetByPK() throws Exception {
        String expected = "Agatha MacDonald";
        String actual = authorDAO.getByPK(-1L).getName();
        assertEquals(expected, actual);
        assertNull(authorDAO.getByPK(100L));
    }

    @Test(expected = Exception.class)
    public void testDelete() throws Exception {
        Long expected = -10L;
        authorDAO.delete(expected);
        assertNull(authorDAO.getByPK(-10L));
        authorDAO.delete(expected);//To check exception is thrown
    }

    @Test(expected = Exception.class)
    public void testUpdate() throws Exception {
        Author expected = new Author();
        expected.setId(-1L);
        expected.setName("David");
        expected.setExpiredDate(Timestamp.valueOf(LocalDateTime.now()));
        authorDAO.update(expected);
        Author actual = authorDAO.getByPK(expected.getId());
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getExpiredDate(), actual.getExpiredDate());
        expected.setId(10000L);//non-existent author
        authorDAO.update(expected);
    }

    @Test
    @DataSet("test/dao/AuthorDAO/test-dataset.AuthorDAO.xml")
    public void testGetAll() throws Exception {
        Author author1 = new Author();
        author1.setId(22L);
        author1.setName("Yahor");
        author1.setExpiredDate(Timestamp.valueOf("2016-04-19 09:58:44.316"));
        Author author2 = new Author();
        author2.setId(33L);
        author2.setName("Petrov");
        author2.setExpiredDate(Timestamp.valueOf("2016-04-19 09:58:44.316"));
        Author author3 = new Author();
        author3.setId(44L);
        author3.setName("Vodkin");
        List<Author> expected = new ArrayList<>();
        expected.add(author1);
        expected.add(author2);
        expected.add(author3);
        List<Author> actual = authorDAO.getAll();
        assertTrue(expected.containsAll(actual));
    }

    @Test(expected = Exception.class)
    public void testLinkWithNews() throws Exception {
        int expected = 1;
        int actual = authorDAO.linkWithNews(-1L, -5L);
        assertEquals(expected, actual);
        authorDAO.linkWithNews(100L, 300L); //Not existed keys
        authorDAO.linkWithNews(-1L, -1L);//Record already have been existed
    }

    @Test(expected = Exception.class)
    public void testUnLinkWithNews() throws Exception {
        int expected = 2;
        int actual = authorDAO.unLinkWithNews(-2L);
        assertEquals(expected, actual);
        authorDAO.unLinkWithNews(1L);//Not existed key
    }

    @Test
    @DataSet("test/dao/AuthorDAO/test-dataset.AuthorDAO.xml")
    public void testGetActualAuthors() throws Exception {
        Author author1 = new Author();
        author1.setId(44L);
        author1.setName("Vodkin");
        List<Author> expected = new ArrayList<>();
        expected.add(author1);
        List<Author> actual = authorDAO.getActualAuthors();
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testGetByNewsId() throws Exception {
        Author expected = new Author();
        expected.setId(-1L);
        expected.setName("Agatha MacDonald");
        expected.setExpiredDate(Timestamp.valueOf("2017-08-13 09:58:44.316"));
        Author actual = authorDAO.getByNewsId(-1l);
        assertEquals(expected, actual);
    }

}
