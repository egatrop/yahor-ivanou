package com.epam.ivanou.dao.impl.jdbc;

import com.epam.ivanou.dao.ICommentDAO;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.exception.DAOException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

@SpringApplicationContext({"spring-config.xml"})
@Transactional(TransactionMode.ROLLBACK)
@DataSet(value = "test/dao/test-dataset.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class CommentDAOTest extends UnitilsJUnit4 {

    @SpringBean("commentDAO")
    private ICommentDAO commentDAO;

    @BeforeClass
    public static void setSystemProperty() {
        Properties properties = System.getProperties();
        properties.setProperty("spring.profiles.active", "jdbc");
    }

    @Test
    public void testCreate() throws Exception {
        Comment expected = new Comment();
        expected.setId(1L);
        expected.setNewsId(-1l);
        expected.setText("Awesome!!!!!");
        expected.setCreationDate(Timestamp.valueOf("2017-08-13 09:58:44.316"));
        Comment actual = commentDAO.create(expected);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetByPK() throws Exception {
        String expected = "GOOD NEWS";
        String actual = commentDAO.getByPK(-1L).getText();
        assertEquals(expected, actual);
        assertNull(commentDAO.getByPK(100L));
    }

    @Test(expected = DAOException.class)
    public void testUpdate() throws Exception {
        Comment expected = new Comment();
        expected.setId(-1L);
        expected.setNewsId(-1L);//News id should be passed because of NullPointerException
        expected.setText("WOW!");//NewsId couldn't be changed!
        expected.setCreationDate(Timestamp.valueOf(LocalDateTime.now()));
        commentDAO.update(expected);
        Comment actual = commentDAO.getByPK(expected.getId());
        assertEquals(expected, actual);
        expected.setId(10000L);//non-existent comment
        commentDAO.update(expected);
    }

    @Test(expected = DAOException.class)
    public void testDelete() throws Exception {
        Long expected = -6L;
        commentDAO.delete(expected);
        assertNull(commentDAO.getByPK(-6L));
        commentDAO.delete(expected);//To check exception is thrown
    }

    @Test
    @DataSet("test/dao/CommentDAO/test-dataset.CommentDAO.xml")
    public void testGetAll() throws Exception {
        Comment comment1 = new Comment();
        comment1.setId(1L);
        comment1.setNewsId(1L);
        comment1.setText("Bad news!");
        comment1.setCreationDate(Timestamp.valueOf("2016-04-19 09:58:44.316"));
        Comment comment2 = new Comment();
        comment2.setId(2L);
        comment2.setNewsId(1L);
        comment2.setText("Cooool!");
        comment2.setCreationDate(Timestamp.valueOf("2016-04-19 09:59:44.316"));
        Comment comment3 = new Comment();
        comment3.setId(3L);
        comment3.setNewsId(1L);
        comment3.setText("Good news!");
        comment3.setCreationDate(Timestamp.valueOf("2016-04-19 10:58:44.316"));
        Comment comment4 = new Comment();
        comment4.setId(4L);
        comment4.setNewsId(2L);
        comment4.setText("WTFK?!");
        comment4.setCreationDate(Timestamp.valueOf("2016-04-19 09:58:44.316"));
        List<Comment> expected = new ArrayList<>();
        expected.add(comment1);
        expected.add(comment2);
        expected.add(comment3);
        expected.add(comment4);
        List<Comment> actual = commentDAO.getAll();
        assertTrue(expected.containsAll(actual));
    }

    @Test
    @DataSet("test/dao/CommentDAO/test-dataset.CommentDAO.xml")
    public void testDeleteAllByNewsId() throws Exception {
        int expected = 3;
        int actual = commentDAO.deleteAllByNewsId(1L);
        assertEquals(expected, actual);
    }

    @Test
    @DataSet("test/dao/CommentDAO/test-dataset.CommentDAO.xml")
    public void testGetAllByNewsId() throws Exception {
        Comment comment1 = new Comment();
        comment1.setId(1L);
        comment1.setNewsId(1L);
        comment1.setText("Bad news!");
        comment1.setCreationDate(Timestamp.valueOf("2016-04-19 09:58:44.316"));
        Comment comment2 = new Comment();
        comment2.setId(2L);
        comment2.setNewsId(1L);
        comment2.setText("Cooool!");
        comment2.setCreationDate(Timestamp.valueOf("2016-04-19 09:59:44.316"));
        Comment comment3 = new Comment();
        comment3.setId(3L);
        comment3.setNewsId(1L);
        comment3.setText("Good news!");
        comment3.setCreationDate(Timestamp.valueOf("2016-04-19 10:58:44.316"));
        List<Comment> expected = new ArrayList<>();
        expected.add(comment1);
        expected.add(comment2);
        expected.add(comment3);
        List<Comment> actual = commentDAO.getAllByNewsId(1L);
        assertTrue(expected.containsAll(actual));
    }

}