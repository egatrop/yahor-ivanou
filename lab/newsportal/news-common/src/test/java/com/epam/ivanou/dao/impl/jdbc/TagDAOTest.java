package com.epam.ivanou.dao.impl.jdbc;

import com.epam.ivanou.exception.DAOException;
import com.epam.ivanou.dao.ITagDAO;
import com.epam.ivanou.entity.Tag;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

@Configuration
@SpringApplicationContext({"classpath:spring-config.xml"})
@Transactional(TransactionMode.ROLLBACK)
@DataSet(value = "test/dao/test-dataset.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class TagDAOTest extends UnitilsJUnit4 {

    @SpringBean("tagDAO")
    private ITagDAO tagDAO;

    @BeforeClass
    public static void setSystemProperty() {
        Properties properties = System.getProperties();
        properties.setProperty("spring.profiles.active", "jdbc");
    }

    @Test
    public void testCreate() throws Exception {
        Tag expected = new Tag();
        expected.setId(1L);
        expected.setName("HELP");
        Tag actual = tagDAO.create(expected);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetByPK() throws Exception {
        String expected = "Bangkok";
        String actual = tagDAO.getByPK(-1L).getName();
        assertEquals(expected, actual);
        assertNull(tagDAO.getByPK(100L));
    }

    @Test(expected = DAOException.class)
    public void testUpdate() throws Exception {
        Tag expected = new Tag();
        expected.setId(-1L);
        expected.setName("Minsk");
        tagDAO.update(expected);
        Tag actual = tagDAO.getByPK(expected.getId());
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        expected.setId(10000L);//non-existent tag
        tagDAO.update(expected);
    }

    @Test(expected = DAOException.class)
    public void testDelete() throws Exception {
        Long expected = -8L;
        tagDAO.unLinkWithNewsOnTagDelete(expected);
        tagDAO.delete(expected);
        assertNull(tagDAO.getByPK(-8L));
        tagDAO.delete(expected);//To check exception is thrown
    }

    @Test
    @DataSet("test/dao/TagDAO/test-dataset.TagDAO.xml")
    public void testGetAll() throws Exception {
        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setName("WORK");
        Tag tag2 = new Tag();
        tag2.setId(2L);
        tag2.setName("HOME");
        Tag tag3 = new Tag();
        tag3.setId(3L);
        tag3.setName("TRAVELLING");
        List<Tag> expected = new ArrayList<>();
        expected.add(tag1);
        expected.add(tag2);
        expected.add(tag3);
        List<Tag> actual = tagDAO.getAll();
        assertTrue(actual.containsAll(expected));
    }

    @Test(expected = DAOException.class)
    public void testLinkWithNews() throws Exception {
        int expected = 1;
        int actual = tagDAO.linkWithNews(-1L, -8L);
        assertEquals(expected, actual);
        tagDAO.linkWithNews(100L, 300L); //Not existed keys
        tagDAO.linkWithNews(-1L, -1L);//Record already have been existed
    }

    @Test(expected = DAOException.class)
    public void testUnLinkWithNewsOnNewsDelete() throws Exception {
        int expected = 3;
        int actual = tagDAO.unLinkWithNewsOnNewsDelete(-3L);
        assertEquals(expected, actual);
        tagDAO.unLinkWithNewsOnNewsDelete(1L);//Not existed key
    }

    @Test
    public void testGetAllByNewsId() throws Exception {
        Tag tag1 = new Tag();
        tag1.setId(-1L);
        tag1.setName("Bangkok");
        Tag tag2 = new Tag();
        tag2.setId(-2L);
        tag2.setName("ErawanShrine");
        List<Tag> expected = new ArrayList<>();
        expected.add(tag1);
        expected.add(tag2);
        List<Tag> actual = tagDAO.getAllByNewsId(-1l);
        assertTrue(expected.containsAll(actual));
    }
}