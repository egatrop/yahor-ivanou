package com.epam.ivanou.dao.impl.hibernate;

import com.epam.ivanou.dao.ICommentDAO;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.exception.DAOException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

@SpringApplicationContext({"spring-config.xml"})
@Transactional(TransactionMode.ROLLBACK)
@DataSet(value = "test/dao/test-dataset.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class CommentDAOTest extends UnitilsJUnit4 {

    @SpringBean("commentDAO")
    private ICommentDAO commentDAO;

    @BeforeClass
    public static void setSystemProperty() {
        Properties properties = System.getProperties();
        properties.setProperty("spring.profiles.active", "hibernate");
    }

    @Test
    public void testCreate() throws Exception {
        Comment expected = new Comment();
        expected.setNewsId(-1l);
        expected.setText("Awesome!!!!!");
        expected.setCreationDate(Timestamp.valueOf("2017-08-13 09:58:44.316"));
        Comment actual = commentDAO.create(expected);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetByPK() throws Exception {
        String expected = "GOOD NEWS";
        String actual = commentDAO.getByPK(-1L).getText();
        assertEquals(expected, actual);
        assertNull(commentDAO.getByPK(100L));
    }

    @Test
    public void testUpdate() throws Exception {
        Comment expected = new Comment();
        expected.setId(-1L);
        expected.setNewsId(-1L);//News id should be passed because of NullPointerException
        expected.setText("WOW!");//NewsId couldn't be changed!
        expected.setCreationDate(Timestamp.valueOf(LocalDateTime.now()));
        commentDAO.update(expected);
        Comment actual = commentDAO.getByPK(expected.getId());
        assertEquals(expected, actual);
    }

    @Test
    public void testDelete() throws Exception {
        Long expected = -6L;
        commentDAO.delete(expected);
        assertNull(commentDAO.getByPK(-6L));
    }
}