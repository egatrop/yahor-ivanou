package com.epam.ivanou.service.impl.jdbc;

import com.epam.ivanou.entity.Author;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.entity.News;
import com.epam.ivanou.entity.Tag;
import com.epam.ivanou.service.IAuthorService;
import com.epam.ivanou.service.ICommentService;
import com.epam.ivanou.service.INewsService;
import com.epam.ivanou.service.ITagService;
import com.epam.ivanou.service.impl.jdbc.NewsManagementJDBCService;
import com.epam.ivanou.utils.NewsValueObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.unitils.UnitilsJUnit4;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class NewsManagementServiceTest extends UnitilsJUnit4 {

    @Mock
    private INewsService mockedNewsService;
    @Mock
    private ITagService mockedTagService;
    @Mock
    private ICommentService mockedCommentService;
    @Mock
    private IAuthorService mockedAuthorService;

    private NewsManagementJDBCService newsManagementService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        newsManagementService = new NewsManagementJDBCService();
        newsManagementService.setAuthorService(mockedAuthorService);
        newsManagementService.setNewsService(mockedNewsService);
        newsManagementService.setCommentService(mockedCommentService);
        newsManagementService.setTagService(mockedTagService);
        assertNotNull(mockedNewsService);
        assertNotNull(mockedTagService);
        assertNotNull(mockedAuthorService);
        assertNotNull(mockedCommentService);
        assertNotNull(newsManagementService);
    }

    @Test
    public void testAddNews() throws Exception {
        NewsValueObject object = new NewsValueObject();
        News news = new News();
        Author author = new Author();
        author.setId(2L);
        Tag tag1 = new Tag();
        tag1.setId(3L);
        Tag tag2 = new Tag();
        tag2.setId(4L);
        List<Tag> tags = new ArrayList<>();
        tags.add(tag1);
        tags.add(tag2);
        object.setAuthor(author);
        object.setNews(news);
        object.setTags(tags);
        when(mockedNewsService.add(news)).thenReturn(1L);
        news.setId(newsManagementService.addNews(object));
        verify(mockedNewsService, times(1)).add(news);
        verify(mockedAuthorService, times(1)).linkWithNews(news.getId(), author.getId());
        verify(mockedTagService, times(1)).linkWithNews(news.getId(), tag1.getId());
        verify(mockedTagService, times(1)).linkWithNews(news.getId(), tag2.getId());
    }

    @Test
    public void testEditNews() throws Exception {
        NewsManagementJDBCService spy = spy((NewsManagementJDBCService) newsManagementService);
        NewsValueObject currentValueObject = new NewsValueObject();
        doReturn(currentValueObject).when(spy).getNewsById(1L);
        News news1 = new News();
        news1.setId(1L);
        news1.setTitle("Title1");
        news1.setShortText("Short Text1");
        Author author1 = new Author();
        author1.setId(1L);
        author1.setName("Yahor");
        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setName("tag1");
        Tag tag2 = new Tag();
        tag2.setId(2L);
        tag2.setName("tag2");

        currentValueObject.setNews(news1);
        currentValueObject.setAuthor(author1);
        currentValueObject.addTag(tag1);
        currentValueObject.addTag(tag2);

        NewsValueObject newValueObject = new NewsValueObject();
        News news2 = new News();
        news2.setId(1L);
        news2.setTitle("Title2");
        news2.setShortText("Short Text2");
        Author author2 = new Author();
        author2.setId(2L);
        author2.setName("Petr");
        Tag tag3 = new Tag();
        tag3.setId(3L);
        tag3.setName("tag3");
        Tag tag4 = new Tag();
        tag4.setId(4L);
        tag4.setName("tag4");
        Tag tag5 = new Tag();
        tag5.setId(5L);
        tag5.setName("tag5");

        newValueObject.setNews(news2);
        newValueObject.setAuthor(author2);
        newValueObject.addTag(tag3);
        newValueObject.addTag(tag4);
        newValueObject.addTag(tag5);
        newValueObject.addTag(tag1);
        newValueObject.addTag(tag2);

        spy.editNews(newValueObject);

        verify(spy, times(1)).getNewsById(1L);
        verify(mockedNewsService, times(1)).update(news2);
        verify(mockedAuthorService, times(1)).unlinkWithNews(1L);
        verify(mockedAuthorService, times(1)).linkWithNews(1L, author2.getId());
        verify(mockedTagService, times(1)).unlinkWithNews(1L);
        verify(mockedTagService, times(1)).linkWithNews(1L, tag3.getId());
        verify(mockedTagService, times(1)).linkWithNews(1L, tag4.getId());
        verify(mockedTagService, times(1)).linkWithNews(1L, tag5.getId());
        verify(mockedTagService, times(1)).linkWithNews(1L, tag1.getId());
        verify(mockedTagService, times(1)).linkWithNews(1L, tag2.getId());
    }

    @Test
    public void testDeleteNewsById() throws Exception {
        List<Long> idList = new ArrayList<>();
        idList.add(1L);
        idList.add(2L);
        idList.add(3L);
        newsManagementService.deleteNewsById(idList);
        verify(mockedAuthorService, times(3)).unlinkWithNews(anyLong());
        verify(mockedTagService, times(3)).unlinkWithNews(anyLong());
        verify(mockedCommentService, times(3)).deleteAllByNewsId(anyLong());
        verify(mockedNewsService, times(3)).delete(anyLong());
    }

    @Test
    public void testGetNewsById() throws Exception {
        News news = new News();
        news.setId(1L);
        news.setFullText("Hello World!");
        Author author = new Author();
        author.setId(2L);
        author.setName("Yahor");
        Comment comment1 = new Comment();
        comment1.setId(1L);
        comment1.setText("Super");
        Comment comment2 = new Comment();
        comment2.setId(2L);
        comment2.setText("Super!");
        List<Comment> comments = new ArrayList<>();
        comments.add(comment1);
        comments.add(comment2);
        List<Tag> tags = new ArrayList<>();
        tags.add(new Tag());
        tags.add(new Tag());
        tags.add(new Tag());
        NewsValueObject expected = new NewsValueObject();
        expected.setNews(news);
        expected.setAuthor(author);
        expected.setComments(comments);
        expected.setTags(tags);
        when(mockedNewsService.get(1L)).thenReturn(news);
        when(mockedAuthorService.getByNewsId(1L)).thenReturn(author);
        when(mockedTagService.getAllByNewsId(1L)).thenReturn(tags);
        when(mockedCommentService.getAllByNewsId(1L)).thenReturn(comments);
        NewsValueObject actual = newsManagementService.getNewsById(1L);
        verify(mockedNewsService, times(1)).get(1L);
        verify(mockedAuthorService, times(1)).getByNewsId(1L);
        verify(mockedTagService, times(1)).getAllByNewsId(1L);
        verify(mockedCommentService, times(1)).getAllByNewsId(1L);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetAllNews() throws Exception {
        List<News> newsList = new ArrayList<>();
        newsList.add(new News());
        newsList.add(new News());
        newsList.add(new News());
        newsList.get(0).setId(1L);
        newsList.get(1).setId(3L);
        newsList.get(2).setId(2L);
        NewsValueObject valueObject1 = new NewsValueObject();
        valueObject1.setNews(newsList.get(0));
        NewsValueObject valueObject2 = new NewsValueObject();
        valueObject2.setNews(newsList.get(1));
        NewsValueObject valueObject3 = new NewsValueObject();
        valueObject3.setNews(newsList.get(2));
        List<NewsValueObject> expected = new ArrayList<>();
        expected.add(valueObject1);
        expected.add(valueObject2);
        expected.add(valueObject3);
        NewsManagementJDBCService spy = spy((NewsManagementJDBCService) newsManagementService);
        when(mockedNewsService.getAll()).thenReturn(newsList);
        doReturn(expected.get(0)).when(spy).getNewsById(1L);
        doReturn(expected.get(1)).when(spy).getNewsById(2L);
        doReturn(expected.get(2)).when(spy).getNewsById(3L);
        List<NewsValueObject> actual = spy.getAllNews();
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testSearchNews() throws Exception {
        List<News> newsList = new ArrayList<>();
        newsList.add(new News());
        newsList.add(new News());
        newsList.add(new News());
        newsList.get(0).setId(1L);
        newsList.get(1).setId(3L);
        newsList.get(2).setId(2L);
        NewsValueObject valueObject1 = new NewsValueObject();
        valueObject1.setNews(newsList.get(0));
        NewsValueObject valueObject2 = new NewsValueObject();
        valueObject2.setNews(newsList.get(1));
        NewsValueObject valueObject3 = new NewsValueObject();
        valueObject3.setNews(newsList.get(2));
        List<NewsValueObject> expected = new ArrayList<>();
        expected.add(valueObject1);
        expected.add(valueObject2);
        expected.add(valueObject3);
        NewsManagementJDBCService spy = spy((NewsManagementJDBCService) newsManagementService);
        when(mockedNewsService.search(anyObject())).thenReturn(newsList);
        doReturn(expected.get(0)).when(spy).getNewsById(1L);
        doReturn(expected.get(1)).when(spy).getNewsById(2L);
        doReturn(expected.get(2)).when(spy).getNewsById(3L);
        List<NewsValueObject> actual = spy.searchNews(anyObject());
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testCountAllNews() throws Exception {
        when(mockedNewsService.countAll()).thenReturn(10);
        int expected = 10;
        int actual = newsManagementService.countAllNews();
        verify(mockedNewsService, times(1)).countAll();
        assertEquals(expected, actual);
    }

    @Test
    public void testAddAuthor() throws Exception {
        Author author = new Author();
        when(mockedAuthorService.add(author)).thenReturn(1L);
        Long expected = 1L;
        Long actual = newsManagementService.addAuthor(author);
        verify(mockedAuthorService, times(1)).add(author);
        assertEquals(expected, actual);
    }

    @Test
    public void testEditAuthor() throws Exception {
        Author author = new Author();
        newsManagementService.editAuthor(author);
        verify(mockedAuthorService, times(1)).update(author);
    }

    @Test
    public void testExpireAuthor() throws Exception {
        NewsManagementJDBCService spy = spy((NewsManagementJDBCService) newsManagementService);
        Author author = new Author();
        spy.expireAuthor(author);
        verify(spy, times(1)).editAuthor(author);
        newsManagementService.expireAuthor(author);
        verify(mockedAuthorService, times(2)).update(author);
    }

    @Test
    public void testGetAllAuthors() throws Exception {
        newsManagementService.getAllAuthors();
        verify(mockedAuthorService, times(1)).getAll();
    }

    @Test
    public void testAddTag() throws Exception {
        Tag tag = new Tag();
        when(mockedTagService.add(tag)).thenReturn(1L);
        Long expected = 1L;
        Long actual = newsManagementService.addTag(tag);
        verify(mockedTagService, times(1)).add(tag);
        assertEquals(expected, actual);
    }

    @Test
    public void testEditTag() throws Exception {
        Tag tag = new Tag();
        newsManagementService.editTag(tag);
        verify(mockedTagService, times(1)).update(tag);
    }

    @Test
    public void testDeleteTag() throws Exception {
        newsManagementService.deleteTag(1L);
        verify(mockedTagService, times(1)).delete(1L);
    }

    @Test
    public void testGetAllTags() throws Exception {
        List<Tag> expected = new ArrayList<>();
        when(mockedTagService.getAll()).thenReturn(expected);
        List<Tag> actual = newsManagementService.getAllTags();
        verify(mockedTagService, times(1)).getAll();
        assertEquals(expected, actual);
    }

    @Test
    public void testCommentGetById() throws Exception {
        Comment expected = new Comment();
        expected.setId(1L);
        when(mockedCommentService.get(1L)).thenReturn(expected);
        Comment actual = newsManagementService.getCommentById(1L);
        verify(mockedCommentService, times(1)).get(1L);
        assertEquals(expected, actual);
    }

    @Test
    public void testAddComment() throws Exception {
        Comment comment = new Comment();
        when(mockedCommentService.add(comment)).thenReturn(1L);
        Long expected = 1L;
        Long actual = newsManagementService.addComment(comment);
        verify(mockedCommentService, times(1)).add(comment);
        assertEquals(expected, actual);
    }

    @Test
    public void testDeleteComment() throws Exception {
        newsManagementService.deleteComment(1L);
        verify(mockedCommentService, times(1)).delete(1L);
    }
}