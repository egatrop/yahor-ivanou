package com.epam.ivanou.service.impl.jdbc;

import com.epam.ivanou.dao.ICommentDAO;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.service.ICommentService;
import com.epam.ivanou.service.impl.jdbc.CommentJDBCService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.unitils.UnitilsJUnit4;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class CommentServiceTest extends UnitilsJUnit4 {

    @Mock
    private ICommentDAO mokckedCommentDAO;

    private ICommentService commentService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        commentService = new CommentJDBCService(mokckedCommentDAO);
        assertNotNull(mokckedCommentDAO);
        assertNotNull(commentService);
    }

    @Test
    public void testGetById() throws Exception {
        Comment expected = new Comment();
        expected.setId(1L);
        when(mokckedCommentDAO.getByPK(1L)).thenReturn(expected);
        Comment actual = commentService.get(1L);
        verify(mokckedCommentDAO, times(1)).getByPK(1L);
        assertEquals(expected, actual);
    }

    @Test
    public void testAdd() throws Exception {
        Long expected = 1L;
        Comment comment = new Comment();
        comment.setId(1l);
        when(mokckedCommentDAO.create(comment)).thenReturn(comment);
        Long actual = commentService.add(comment);
        verify(mokckedCommentDAO, times(1)).create(comment);
        assertEquals(expected, actual);
    }

    @Test
    public void testDelete() throws Exception {
        commentService.delete(1L);
        verify(mokckedCommentDAO, times(1)).delete(1L);
    }

    @Test
    public void testDeleteAllByNewsId() throws Exception {
        when(mokckedCommentDAO.deleteAllByNewsId(1L)).thenReturn(5);
        int expected = 5;
        int actual = commentService.deleteAllByNewsId(1L);
        verify(mokckedCommentDAO, times(1)).deleteAllByNewsId(1L);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetAllByNewsId() throws Exception {
        ArrayList<Comment> comments = new ArrayList<>();
        comments.add(new Comment());
        comments.add(new Comment());
        comments.add(new Comment());
        when(mokckedCommentDAO.getAllByNewsId(1L)).thenReturn(comments);
        int expected = 3;
        int actual = commentService.getAllByNewsId(1L).size();
        verify(mokckedCommentDAO, times(1)).getAllByNewsId(1L);
        assertEquals(expected, actual);
    }
}