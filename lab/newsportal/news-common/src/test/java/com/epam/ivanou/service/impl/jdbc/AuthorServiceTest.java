package com.epam.ivanou.service.impl.jdbc;

import com.epam.ivanou.dao.IAuthorDAO;
import com.epam.ivanou.entity.Author;
import com.epam.ivanou.service.IAuthorService;
import com.epam.ivanou.service.impl.jdbc.AuthorJDBCService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.unitils.UnitilsJUnit4;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class AuthorServiceTest extends UnitilsJUnit4 {

    @Mock
    private IAuthorDAO mokckedAuthorDAO;

    private IAuthorService authorService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        authorService = new AuthorJDBCService(mokckedAuthorDAO);
        assertNotNull(mokckedAuthorDAO);
        assertNotNull(authorService);
    }

    @Test
    public void testAdd() throws Exception {
        Long expected = 1L;
        Author author = new Author();
        author.setId(1l);
        when(mokckedAuthorDAO.create(author)).thenReturn(author);
        Long actual = authorService.add(author);
        verify(mokckedAuthorDAO, times(1)).create(author);
        assertEquals(expected, actual);
    }

    @Test
    public void testUpdate() throws Exception {
        Author author = new Author();
        author.setName("Yahor");
        authorService.update(author);
        verify(mokckedAuthorDAO).update(author);
    }

    @Test
    public void testGetAll() throws Exception {
        ArrayList<Author> authors = new ArrayList<>();
        authors.add(new Author());
        authors.add(new Author());
        authors.add(new Author());
        when(mokckedAuthorDAO.getAll()).thenReturn(authors);
        int expected = 3;
        int actual = authorService.getAll().size();
        verify(mokckedAuthorDAO, times(1)).getAll();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetByNewsId() throws Exception {
        Author expected = anyObject();
        when(mokckedAuthorDAO.getByNewsId(1L)).thenReturn(expected);
        Author actual = authorService.getByNewsId(1L);
        verify(mokckedAuthorDAO, times(1)).getByNewsId(1L);
        assertEquals(expected, actual);
    }

    @Test
    public void testLinkWithNews() throws Exception {
        when(mokckedAuthorDAO.linkWithNews(1L, 2L)).thenReturn(1);
        int expected = 1;
        int actual = authorService.linkWithNews(1l, 2l);
        verify(mokckedAuthorDAO, times(1)).linkWithNews(1L, 2L);
        assertEquals(expected, actual);
    }

    @Test
    public void testUnlinkWithNews() throws Exception {
        when(mokckedAuthorDAO.unLinkWithNews(1L)).thenReturn(5);
        int expected = 5;
        int actual = authorService.unlinkWithNews(1l);
        verify(mokckedAuthorDAO, times(1)).unLinkWithNews(1L);
        assertEquals(expected, actual);
    }
}