package com.epam.ivanou;

import com.epam.ivanou.pages.HomePage;
import com.epam.ivanou.pages.LoginPage;
import com.epam.ivanou.service.impl.jdbc.NewsManagementJDBCService;
import org.apache.log4j.Logger;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.apache.wicket.spring.test.ApplicationContextMock;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Properties;

import static org.mockito.Mockito.mock;


public class TestHomePage extends WicketApplication {

    private static final Logger log = Logger.getLogger(WicketApplication.class);
    private static WicketTester tester;
    private static ApplicationContextMock ctx;
    public static NewsManagementJDBCService newsManagementServiceMock;

    @BeforeClass
    public static void setupTester() {
        newsManagementServiceMock = mock(NewsManagementJDBCService.class);
        ctx = new ApplicationContextMock();
        ctx.putBean("newsManagementService", newsManagementServiceMock);
        tester = new WicketTester(new TestHomePage());
    }

    @Before
    public void setupTest() {
        Properties properties = System.getProperties();
        properties.setProperty("spring.profiles.active", "hibernate");
    }

    @Test
    public void homepageRendersSuccessfully() {
        //start and render the test page
        tester.startPage(HomePage.class);

        //assert rendered page class
        tester.assertRenderedPage(HomePage.class);
    }

    @Test
    public void loginPageRenderSuccessfully() {
        tester.startPage(LoginPage.class);
        tester.assertRenderedPage(LoginPage.class);
    }

    @Override
    protected void initSpringInjector() {
        log.debug("inside initSpringInjector() in TEST ctx=" + ctx);
        getComponentInstantiationListeners().add(new SpringComponentInjector(
                this, ctx));
    }
}
