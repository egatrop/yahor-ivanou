package com.epam.ivanou.layouts;

import com.epam.ivanou.pages.HomePage;
import com.epam.ivanou.entity.Author;
import com.epam.ivanou.entity.Tag;
import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.service.INewsManagementService;
import com.epam.ivanou.utils.SearchCriteria;
import org.apache.log4j.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.markup.html.IHeaderResponse;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.ListMultipleChoice;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FilterForm extends Form {

    private static final Logger log = Logger.getLogger(FilterForm.class);

    @SpringBean(name = "newsManagementService")
    private INewsManagementService service;

    public FilterForm(String id) {
        super(id);
        Injector.get().inject(this);
        ChoiceRenderer<Author> authorChoiceRenderer = new ChoiceRenderer<>("name", "id");

        LoadableDetachableModel<Author> authorModel = new LoadableDetachableModel<Author>() {

            private Long authorId;

            @Override
            protected Author load() {
                Author author = new Author();
                if (authorId != null) {
                    try {
                        author = service.getAuthorById(authorId);
                    } catch (ServiceException e) {
                        log.error("Exception during retrieving the author from the database.", e);
                    }
                }
                authorId = null;
                return author;
            }

            @Override
            public void setObject(Author object) {
                super.setObject(object);
                if (object != null) {
                    authorId = object.getId();
                }
            }
        };
        //Dropdown for author choice
        add(new DropDownChoice<>("authors",
                        authorModel,
                        new LoadableDetachableModel<List<? extends Author>>() {
                            @Override
                            protected List<? extends Author> load() {
                                List<Author> authors = Collections.<Author>emptyList();
                                try {
                                    authors = service.getAllAuthors();
                                } catch (ServiceException e) {
                                    log.error("Exception during retrieving all the authors " +
                                            "from the database.", e);
                                }
                                return authors;
                            }
                        },
                        authorChoiceRenderer).setNullValid(true)
        );

        LoadableDetachableModel<List<Tag>> tagModel = new LoadableDetachableModel<List<Tag>>() {

            private List<Long> tagsId;

            @Override
            protected List<Tag> load() {
                List<Tag> tags = new ArrayList<>();
                if (tagsId == null) {
                    tagsId = new ArrayList<>();
                }
                try {
                    for (Long id : tagsId) {
                        tags.add(service.getTagById(id));
                    }
                } catch (ServiceException e) {
                    log.error("Exception during retrieving the tags from the database.", e);
                }
                tagsId = null;
                return tags;
            }

            @Override
            public void setObject(List<Tag> list) {
                super.setObject(list);
                tagsId = new ArrayList<>();
                for (Tag tag : list) {
                    tagsId.add(tag.getId());
                }
            }
        };
        //DropdownMultiple for tags choice
        ChoiceRenderer<Tag> tagChoiceRenderer = new ChoiceRenderer<>("name", "id");
        ListMultipleChoice<Tag> multipleChoice = new ListMultipleChoice<Tag>("tags",
                tagModel,
                new LoadableDetachableModel<List<? extends Tag>>() {
                    @Override
                    protected List<? extends Tag> load() {
                        List<Tag> tags = Collections.<Tag>emptyList();
                        try {
                            tags = service.getAllTags();
                        } catch (ServiceException e) {
                            log.error("Exception during retrieving all the tags from the database.", e);
                        }
                        return tags;
                    }
                },
                tagChoiceRenderer);
        add(multipleChoice);
        //Filter button
        add(new AjaxSubmitLink("doFilter") {
                @Override
                protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                    log.debug("AjaxSubmitLink#onSubmit()");
                    SearchCriteria criteria = new SearchCriteria();
                    Author selectedAuthor = (Author) authorModel.getObject();
                    if (selectedAuthor != null ) {
                        Long filteredAuthorId = (selectedAuthor.getId());
                        criteria.setAuthorId(filteredAuthorId);
                    }
                    ArrayList<Tag> tags = (ArrayList<Tag>) tagModel.getObject();
                    for (Tag tag : tags) {
                        criteria.addTagId(tag.getId());
                    }
                    ((HomePage) getPage()).updatePageDataAndSession(criteria);
                    target.add(((HomePage) getPage()).getNewsContainer());
                }
            }
        );
        //Reset button
        add(new AjaxSubmitLink("reset") {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                setResponsePage(HomePage.class);
            }
        });
    }

    public void setService(INewsManagementService service) {
        this.service = service;
    }
}
