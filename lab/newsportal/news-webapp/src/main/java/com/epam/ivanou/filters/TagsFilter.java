package com.epam.ivanou.filters;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;

public class TagsFilter implements Filter {

    private static final Logger log = Logger.getLogger(TagsFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //NOP
    }
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        log.debug("Inside tagsFilter");
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        JSONArray records = new JSONArray();
        json.put("records", records);
        JSONObject record = new JSONObject();
        record.put("name", "Egor");
        record.put("column1", "Ivanou");
        record.put("column2", "SSS");
        records.put(record);
        out.print(json.toString());
    }

    @Override
    public void destroy() {
    }
}
