package com.epam.ivanou.layouts;

import com.epam.ivanou.pages.DetailedPage;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.exception.ServiceException;
import org.apache.log4j.Logger;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormValidatingBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.time.Duration;
import org.apache.wicket.validation.validator.StringValidator;

import java.sql.Timestamp;
import java.time.LocalDateTime;

//Markup for the component within DetailedPage.html
public class CommentForm extends StatelessForm<Comment> {

    private static final Logger log = Logger.getLogger(CommentForm.class);

    public CommentForm(String id, Long newsId) {
        super(id);
        //Adjusting validators' behavior
        AjaxFormValidatingBehavior.addToAllFormComponents(this, "onkeyup", Duration.ONE_SECOND);

        final FeedbackPanel feedback = new FeedbackPanel("feedback") {
            @Override
            protected void onRender() {
                super.onRender();
                log.debug("FeedbackPanel onRender()");
            }
            @Override
            protected void onConfigure() {
                super.onConfigure();
                log.debug("FeedbackPanel onConfigure()");
            }
            @Override
            public void onEvent(IEvent<?> event) {
                super.onEvent(event);
                log.debug("toString(): " + event.toString() + " type=" + event.getType());
            }
        };
        feedback.setOutputMarkupPlaceholderTag(true);
        add(feedback);

        //TextArea for comment
        FormComponent<String> commentField = new TextArea<String>("commentText", Model.of("")) {
            @Override
            protected void onRender() {
                super.onRender();
                log.debug("TextArea onRender()");
            }

            @Override
            protected void onConfigure() {
                super.onConfigure();
                log.debug("TextArea onConfigure()");
            }

        }.setRequired(true);
        //Validator for comment's length. See Detailed.properties file with customized
        // validator's labels
        commentField.add(StringValidator.lengthBetween(1, 100));
        commentField.add(new AjaxEventBehavior("onblur") {
            @Override
            protected void onEvent(AjaxRequestTarget target) {
                feedback.setVisible(false);
                target.add(feedback);
            }
        });
        commentField.add(new AjaxEventBehavior("onfocus") {
            @Override
            protected void onEvent(AjaxRequestTarget target) {
                feedback.setVisible(true);
                target.add(feedback);
            }
        });
        add(commentField);

        add(new AjaxButton("submit", this) {

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                log.debug("AjaxButton onSubmit()");
                Comment comment = new Comment();
                comment.setCreationDate(Timestamp.valueOf(LocalDateTime.now()));
                comment.setNewsId(newsId);
                comment.setText(commentField.getModelObject());
                try {
                    ((DetailedPage) getPage()).addComment(comment);
                } catch (ServiceException e) {
                    log.error("Couldn't add new comment to the database", e);
                    error("Sorry. Couldn't add the comment.");
                }
                commentField.setDefaultModel(Model.of(""));
                target.add(CommentForm.this, ((DetailedPage) getPage()).getRefreshingViewWrapper());
            }
            //If validators for the form components generate error, then after submitting the form
            //the method will be called and 'onSubmit' will not be called
            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                log.debug("AjaxButton onError()");
                error("Incorrect input!");
                target.add(feedback);
            }
        });
    }

    @Override
    protected void onRender() {
        super.onRender();
        log.debug("StatelessForm onRender() getParent:" + getParent() + "getPage():" + getPage());
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();
        log.debug("StatelessForm onConfigure() getParent:" + getParent() + "getPage():" + getPage());
    }

    @Override
    protected void onValidate() {
        super.onValidate();
        log.debug("StatelessForm onValidate() getParent:" + getParent() + "getPage():" + getPage());
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        log.debug("StatelessForm onInitialize() getParent:" + getParent() + "getPage():" + getPage());
    }
}
