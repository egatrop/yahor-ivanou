package com.epam.ivanou.pages;

import com.epam.ivanou.layouts.Template;
import com.epam.ivanou.service.INewsManagementService;
import org.apache.log4j.Logger;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.IHeaderResponse;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class TagsPage extends Template {

    private static final Logger log = Logger.getLogger(TagsPage.class);

    @SpringBean(name = "newsManagementService")
    private INewsManagementService service;

    public TagsPage() {
        super();
        log.debug("TagsPage constructor");
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        response.renderJavaScriptReference(new PackageResourceReference(HomePage.class,
                "resources/ext-base.js"));
        response.renderJavaScriptReference(new PackageResourceReference(HomePage.class,
                "resources/ext-all.js"));
        response.renderJavaScriptReference(new PackageResourceReference(HomePage.class,
                "resources/grid.js"));
        response.renderCSSReference(new PackageResourceReference(HomePage.class,
                "resources/ext-all.css"));
    }
}
