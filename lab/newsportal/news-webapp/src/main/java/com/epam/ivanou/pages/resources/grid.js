Ext.onReady(function() {

  // Generic fields array to use in both store defs.
  var fields = [
     {name: 'name', mapping : 'name'},
     {name: 'column1', mapping : 'column1'},
     {name: 'column2', mapping : 'column2'}
  ];

    // create the data store
    var gridStore = new Ext.data.JsonStore({
    fields : fields,
    autoLoad : true,
    url: 'tags-data',
    root   : 'records'
    });


  // Column Model shortcut array
  var cols = [
    { id : 'name', header: "Record Name", width: 160, sortable: true, dataIndex: 'name'},
    {header: "column1", width: 50, sortable: true, dataIndex: 'column1'},
    {header: "column2", width: 50, sortable: true, dataIndex: 'column2'}
  ];

  // declare the source Grid
    var grid = new Ext.grid.GridPanel({
    ddGroup          : 'gridDDGroup',
        store            : gridStore,
        columns          : cols,
    enableDragDrop   : true,
        stripeRows       : true,
        autoExpandColumn : 'name',
        width            : 650,
        height           : 325,
    region           : 'west',
        title            : 'Data Grid',
    selModel         : new Ext.grid.RowSelectionModel({singleSelect : true})
    });

  //Simple 'border layout' panel to house both grids
  var displayPanel = new Ext.Panel({
    width    : 650,
    height   : 300,
    layout: 'fit',
    renderTo : 'panel',
    items    : [
      grid
    ],
    bbar    : [
      '->', // Fill
      {
        text    : 'Reset Example',
        handler : function() {
          //refresh source grid

        }
      }
    ]
  });

});
