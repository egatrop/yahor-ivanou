package com.epam.ivanou.pages;

import com.epam.ivanou.Mode;
import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.layouts.FilterForm;
import com.epam.ivanou.layouts.NewsPanel;
import com.epam.ivanou.layouts.Template;
import com.epam.ivanou.models.NewsValueObjectModel;
import com.epam.ivanou.providers.NewsDataProvider;
import com.epam.ivanou.service.INewsManagementService;
import com.epam.ivanou.utils.NewsValueObject;
import com.epam.ivanou.utils.SearchCriteria;
import org.apache.log4j.Logger;
import org.apache.wicket.Component;
import org.apache.wicket.Session;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.IHeaderResponse;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.pages.ExceptionErrorPage;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.ArrayList;
import java.util.List;

public class HomePage extends Template implements Modeable {

    private static final Logger log = Logger.getLogger(HomePage.class);

    @SpringBean(name = "newsManagementService")
    private INewsManagementService service;

    private NewsDataProvider provider;
    private ArrayList<Long> newsIdCache;
    private DataView<NewsValueObject> dataView;
    private WebMarkupContainer newsContainer;
    private Component form;
    private List<Long> newsToDelete = new ArrayList<>();

    public HomePage() {
        super();
        log.debug("HomePage constructor");
        if (!AuthenticatedWebSession.get().isSignedIn()) {
            WebMarkupContainer menuStub = new WebMarkupContainer("menuPanel");
            menuStub.setVisible(false);
            addOrReplace(menuStub);
        }

        initPageDataAndSession();

        //NewsList
        //Container for ajax refreshing
        newsContainer = new WebMarkupContainer("news");
        newsContainer.setOutputMarkupId(true);
        add(newsContainer);
        dataView = new DataView<NewsValueObject>("newsList", provider) {
            @Override
            protected void populateItem(Item<NewsValueObject> item) {
                RepeatingView repeatingView = new RepeatingView("newsItem");
                repeatingView.add(new NewsPanel(repeatingView.newChildId(),
                        (NewsValueObjectModel) item.getModel(),
                        getModes()));
                item.add(repeatingView);
            }
            @Override
            protected void onInitialize() {
                log.debug("DataView onInitialize()");
                super.onInitialize();
            }

            @Override
            protected void onConfigure() {
                super.onConfigure();
                log.debug("DataView onConfigure()");
            }
        };
        dataView.setItemsPerPage(3);
        newsContainer.add(dataView);
        newsContainer.add(new PagingNavigator("pagingNavigator", dataView).setOutputMarkupId(true));

        //FilterForm
        Form form = new FilterForm("filterForm");
        form.setOutputMarkupId(true);
        add(form);

        AjaxLink deleteNewsButton = new AjaxLink("deleteNewsButton") {

            @Override
            public void onClick(AjaxRequestTarget target) {
                log.debug("Delete button for News was pressed");
                try {
                    service.deleteNewsById(newsToDelete);                    setResponsePage(HomePage.class);
                } catch (ServiceException e) {
                    log.error("Exception during deleting the news.");
                }

            }
            @Override
            protected void onConfigure() {
                super.onConfigure();
                setVisible( AuthenticatedWebSession.get().isSignedIn() );
            }
        };
        add(deleteNewsButton);
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        response.renderJavaScriptReference(new PackageResourceReference(HomePage.class,
                "resources/jquery.multiple.select.js"));
        response.renderCSSReference(new PackageResourceReference(HomePage.class,
                "resources/multiple-select.css"));
        response.renderOnDomReadyJavaScript("" +
                "$('select#tags').multipleSelect({placeholder:'Choose tags'});");
    }

    public void updatePageDataAndSession(SearchCriteria criteria) {
        List<NewsValueObject> valueObjects;
        provider = (NewsDataProvider) dataView.getDataProvider();
        try {
            valueObjects = service.searchNews(criteria);
            setPageDataAndSession(valueObjects);
        } catch (ServiceException e) {
            log.error("Exception during searching news.", e);
            setResponsePage(ExceptionErrorPage.class);
        }
    }

    private void initPageDataAndSession() {
        List<NewsValueObject> valueObjects;
        try {
            valueObjects = service.getAllNews();
            setPageDataAndSession(valueObjects);
        } catch (ServiceException e) {
            log.error("Exception during retrieving all the news from the database.", e);
            setResponsePage(ExceptionErrorPage.class);
        }
    }

    private void setPageDataAndSession(List<NewsValueObject> valueObjects) {
        if (provider != null) {
            provider.setData(valueObjects);
        } else {
            provider = new NewsDataProvider(valueObjects);
        }
        newsIdCache = new ArrayList<>();
        for (NewsValueObject object : valueObjects) {
            newsIdCache.add(object.getNews().getId());
        }
        Session session = Session.get();
        session.setAttribute("backPage", this);
        session.setAttribute("newsIdCache", newsIdCache);
    }

    @Override
    public Mode[] getModes() {
        if (AuthenticatedWebSession.get().isSignedIn()) {
            return new Mode[]{Mode.SHORT, Mode.ADMIN};
        } else {
            return new Mode[]{Mode.SHORT, Mode.CLIENT};
        }
    }

    public List<Long> getNewsToDelete() {
        return newsToDelete;
    }

    public WebMarkupContainer getNewsContainer() {
        return newsContainer;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        log.debug("HomePage onInitialize()");
    }

    @Override
    protected void onRender() {
        super.onRender();
        log.debug("HomePage onRender()");
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();
        log.debug("HomePage onConfigure()");
    }
}
