package com.epam.ivanou.layouts;

import org.apache.log4j.Logger;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.springframework.security.core.context.SecurityContextHolder;

public class HeaderPanel extends Panel {

    private static final Logger log = Logger.getLogger(HeaderPanel.class);

    public HeaderPanel(String id) {
        super(id);
        log.debug("HeaderPanel constructor");

        add(new Label("headerTitle", AuthenticatedWebSession.get().isSignedIn() ?
                new ResourceModel("headerPanel.headerTitleForAdmin") :
                new ResourceModel("headerPanel.headerTitleForClient")));

        add(new AbstractLink("logout") {
            @Override
            protected void onConfigure() {
                super.onConfigure();
                setVisible(AuthenticatedWebSession.get().isSignedIn());
            }
        }.add(new Label("logout", new ResourceModel("headerPanel.logout"))));

        add(new Label("adminLogin", "") {
            @Override
            protected void onConfigure() {
                super.onConfigure();
                if (!AuthenticatedWebSession.get().isSignedIn()) {
                    setVisible(false);
                } else {
                    setDefaultModelObject(HeaderPanel.this.getString("headerPanel.greetingWord")
                            + ", " + SecurityContextHolder.getContext().getAuthentication()
                            .getName() + "!");
                }
            }
        });
    }

    @Override
    protected void onInitialize() {
        log.debug("HeaderPanel onInitialize()");
        super.onInitialize();
    }

    @Override
    protected void onRender() {
        super.onRender();
        log.debug("HeaderPanel onRender()");
    }

}
