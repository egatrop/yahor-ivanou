package com.epam.ivanou.layouts;

import com.epam.ivanou.Mode;
import org.apache.log4j.Logger;
import org.apache.wicket.Component;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.TransparentWebMarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class Template extends WebPage {

    private static final Logger log = Logger.getLogger(Template.class);

    private Component headerPanel;
    private Component menuPanel;
    private Component footerPanel;

    public Template() {
        log.debug("Template constructor");
        add(new TransparentWebMarkupContainer("container") {
            @Override
            protected void onComponentTag(ComponentTag tag) {
                if (AuthenticatedWebSession.get().isSignedIn()) {
                    tag.append("class", "positionedLeft250", " ");
                }
                super.onComponentTag(tag);
            }
        });
        add(headerPanel = new HeaderPanel("headerPanel"));
        add(menuPanel = new MenuPanel("menuPanel"));
        add(footerPanel = new FooterPanel("footerPanel"));
    }

    protected Component getHeaderPanel() {
        return headerPanel;
    }

    protected Component getMenuPanel() {
        return menuPanel;
    }

    protected Component getFooterPanel() {
        return footerPanel;
    }


    @Override
    protected void onConfigure() {
        super.onConfigure();
        log.debug("Template onConfigure()");
    }

    @Override
    protected void onRender() {
        super.onRender();
        log.debug("Template onRender()");
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        log.debug("Template onInitialize()");
    }
}