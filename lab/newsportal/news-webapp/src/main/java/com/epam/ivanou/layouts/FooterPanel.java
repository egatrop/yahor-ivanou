package com.epam.ivanou.layouts;


import org.apache.log4j.Logger;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.ResourceModel;

public class FooterPanel extends Panel {

    private static final Logger log = Logger.getLogger(FooterPanel.class);

    public FooterPanel(String id) {
        super(id);
        add(new Label("copyright", new ResourceModel("footerPanel.copyright")));
        log.debug("FooterPanel constructor");

    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        log.debug("FooterPanel onInitialize()");
    }

    @Override
    protected void onRender() {
        super.onRender();
        log.debug("FooterPanel onRender()");
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();
        log.debug("FooterPanel onConfigure()");
    }
}
