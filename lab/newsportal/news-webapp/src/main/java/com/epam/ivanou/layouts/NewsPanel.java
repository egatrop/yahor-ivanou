package com.epam.ivanou.layouts;

import com.epam.ivanou.Mode;
import com.epam.ivanou.models.NewsValueObjectModel;
import com.epam.ivanou.pages.DetailedPage;
import com.epam.ivanou.pages.HomePage;
import com.epam.ivanou.utils.NewsValueObject;
import org.apache.log4j.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.datetime.markup.html.basic.DateLabel;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.io.Serializable;
import java.util.List;

public class NewsPanel extends Panel {

    private static final Logger log = Logger.getLogger(NewsPanel.class);
    private static final long serialVersionUID = 1L;
    private Long newsId;

    public NewsPanel(String id, NewsValueObjectModel model, Mode... panelModes) {
        super(id, model);
        log.debug("NewsPanel constructor");
        NewsValueObject object = (NewsValueObject) getDefaultModelObject();
        newsId = object.getNews().getId();
        CompoundPropertyModel cModel = CompoundPropertyModel.of(getDefaultModel());
        String tags = tagsAsString(object);

        Label title = new Label("title", cModel.bind("news.title"));

        add(new Label("author", cModel.bind("author.name")));
        add(DateLabel.forDatePattern("modificationDate", cModel.bind("news.modificationDate"), "MM/dd/yyyy"));

        switch (panelModes[0]) {
            case FULL:
                add(new WebMarkupContainer("titleWrapper").add(title));
                add(new MultiLineLabel("textArea", cModel.bind("news.fullText")));
                add(new Label("link").setVisible(false));
                add(new Label("editCheckBox").setVisible(false));
                add(new WebMarkupContainer("commentsNumbHolder")
                        .add(new Label("commentsNumb")).setVisible(false));
                add(new Label("tags").setVisible(false));
                break;
            case SHORT:
                PageParameters parameters = new PageParameters();
                parameters.add("newsId", object.getNews().getId());
                add(new MultiLineLabel("textArea", cModel.bind("news.shortText")));
                add(new Label("tags", tags));
                add(new WebMarkupContainer("commentsNumbHolder")
                        .add(new Label("commentsNumb",
                                String.valueOf(object.getComments().size()))));
                switch (panelModes[1]) {
                    case CLIENT:
                        add(new WebMarkupContainer("titleWrapper").add(title));
                        add(new BookmarkablePageLink("link", DetailedPage.class, parameters)
                                .add(new Label("linklabel", "View")));
                        add(new WebMarkupContainer("editCheckBox").setVisible(false));
                        break;
                    case ADMIN:
                        add(new BookmarkablePageLink("titleWrapper", DetailedPage.class,
                                parameters) {

                            @Override
                            protected void onComponentTag(ComponentTag tag) {
                                tag.setName("a");
                                tag.put("class", "titleLink");
                                super.onComponentTag(tag);
                            }
                        }
                                .add(title));

                        add(new BookmarkablePageLink("link", DetailedPage.class, parameters)
                                .add(new Label("linklabel", "Edit")));

                        add(new AjaxCheckBox("editCheckBox",
                                new PropertyModel<Boolean>(new ToDeleteNews(), "checked")) {

                            @Override
                            protected void onBeforeRender() {
                                super.onBeforeRender();
                                setModelObject(((HomePage) getPage()).getNewsToDelete().contains(newsId));
                            }

                            @Override
                            protected void onUpdate(AjaxRequestTarget target) {
                                log.debug("AjaxCheckBox#onUpdate()");
                                List<Long> list = ((HomePage) getPage()).getNewsToDelete();
                                if (getModelObject()) {
                                    list.add(newsId);
                                } else {
                                    list.remove(newsId);
                                }
                            }

                        });
                        break;
                }
                break;
        }
    }

    private String tagsAsString(NewsValueObject object) {
        StringBuilder tagsBuilder = new StringBuilder();
        int tagsAmount = object.getTags().size();
        if (object.getTags().size() != 0) {
            for (int i = 0; i < tagsAmount; i++) {
                tagsBuilder.append(object.getTags().get(i).getName());
                if (i < tagsAmount - 1)
                    tagsBuilder.append(" ");
            }
        }
        return tagsBuilder.toString();
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();
        log.debug("NewsPanel onConfigure()");
    }

    @Override
    protected void onInitialize() {
        log.debug("NewsPanel onInitialize()");
        super.onInitialize();
    }

    @Override
    protected void onRender() {
        super.onRender();
        log.debug("NewsPanel onRender()");
    }


    private class ToDeleteNews implements Serializable {
        public boolean checked;
    }

}