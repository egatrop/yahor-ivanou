package com.epam.ivanou.pages;

import com.epam.ivanou.Mode;

public interface Modeable {
    Mode[] getModes();
}
