Ext.onReady(function() {

    function formatStatus(value) {
        return value ? 'expired' : 'normal';
    }

    function refreshGrid() {
        store.load();
        store.toHireBuffer.length = 0;
        store.toExpireBuffer.length = 0;
        expireAuthorBtn.disable();
        hireAuthorBtn.disable();
        editAuthorBtn.disable();
        addAuthorBtn.enable();
        grid.getView().refresh();
    }

    // shorthand alias
    var fm = Ext.form;

    var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default
        },
        columns: [{
            xtype: 'checkcolumn',
            dataIndex: 'checked',
            width: 55
        }, {
            id: 'name',
            header: 'Author Name',
            dataIndex: 'name',
            width: 220
        }, {
            header: 'Status',
            dataIndex: 'status',
            width: 70,
            align: 'right',
            renderer: formatStatus
        }]
    });

    var store = new Ext.data.JsonStore({
        toHireBuffer: [],
        toExpireBuffer: [],
        toEdit: {},
        autoLoad: true,
        url: 'authors-data',
        root: 'records',
        fields: [{
            name: 'name',
            mapping: 'name'
        }, {
            name: 'status',
            type: 'bool',
            mapping: 'expired'
        }, {
            name: 'checked',
            type: 'bool'
        }],
        listeners: {
            exception: function(proxy, response, operation) {
                Ext.Msg.alert('Fail to load data!');
            },
            update: function(store, record, operation) {
                var expired = record.json.expired;
                var checked = record.data.checked;
                var toHireBuffer = this.toHireBuffer;
                var toExpireBuffer = this.toExpireBuffer;
                if (expired) {
                    checked ? toHireBuffer.push(true) : toHireBuffer.pop();
                } else {
                    checked ? toExpireBuffer.push(true) : toExpireBuffer.pop();
                }

                var toHireBufferLength = toHireBuffer.length;
                var toExpireBufferLength = toExpireBuffer.length;

                if (toHireBufferLength != 0 && toExpireBufferLength == 0) {
                    hireAuthorBtn.enable();
                    addAuthorBtn.disable();
                } else if (toHireBufferLength == 0 && toExpireBufferLength != 0) {
                    expireAuthorBtn.enable();
                    addAuthorBtn.disable();
                } else if (toExpireBufferLength == 0 && toHireBufferLength == 0 || toExpireBufferLength != 0 && toHireBufferLength != 0) {
                    if (toExpireBufferLength == 0 && toHireBufferLength == 0) {
                        addAuthorBtn.enable();
                    }
                    hireAuthorBtn.disable();
                    expireAuthorBtn.disable();
                }

                if ((toExpireBufferLength == 1 && toHireBufferLength == 0) || (toExpireBufferLength == 0 && toHireBufferLength == 1)) {
                    editAuthorBtn.enable();
                    var items = this.data.items;
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].data.checked) {
                            this.toEdit = items[i].json;
                        }
                    }
                } else {
                    editAuthorBtn.disable();
                    this.toEdit = {};
                }
            }
        }
    });

    var addAuthorBtn = new Ext.Button({
        text: 'Add author',
        handler: function() {
            winAdd.show();
        }
    });

    var editAuthorBtn = new Ext.Button({
        text: 'Edit author',
        disabled: true,
        handler: function() {
            var winEdit = new Ext.Window({
                title: 'Edit the author',
                width: 400,
                height: 130,
                layout: 'fit',
                
                shadow: false,
                resizable: false,
                draggable: true,
                closable: false,
                modal: true,

                items: {
                    xtype: 'form',
                    itemId: 'form',
                    id: 'ajaxEditAuthorForm',

                    items: [{
                        xtype: 'textfield',
                        itemId: 'authorNameFld',
                        width: 250,
                        fieldLabel: 'Name',
                        maxLength: 30,
                        allowBlank: false,
                        maxLengthText: 'Max length of name is 30 sym.',
                        blankText: 'Name shuldn\'t be empty',
                        msgTarget: 'under'
                    }, {
                        xtype: 'checkbox',
                        itemId: 'expiredChbx',
                        fieldLabel: 'Expired',
                    }],

                    buttons: [{
                        text: 'Edit',
                        handler: function() {
                            var inputExpired = winEdit.getComponent('form').getComponent('expiredChbx').getValue();
                            var inputName = winEdit.getComponent('form').getComponent('authorNameFld').getValue();
                            var form = winEdit.getComponent('form').getForm();

                            if (form.isValid()) {
                                Ext.Ajax.request({
                                    url: 'edit_author',
                                    method: 'POST',
                                    form: 'ajaxEditAuthorForm',
                                    params: {
                                        id: store.toEdit.id,
                                        name: inputName,
                                        expired: inputExpired
                                    },
                                    success: function(response, opts) {
                                        winEdit.getComponent('form').getForm().reset();
                                        winEdit.hide();
                                        refreshGrid();
                                    },
                                });
                            } else {
                                Ext.MessageBox.alert('Wrong input!', 'The name is incorrect!')
                            }
                        }
                    }, {
                        text: 'Cancel',
                        handler: function() {
                            winEdit.close();
                        }
                    }]
                },

                listeners: {
                    beforerender: function() {
                        this.getComponent('form').getComponent('authorNameFld').value = store.toEdit.name;
                        this.getComponent('form').getComponent('expiredChbx').checked = store.toEdit.expired;
                    },
                }

            });
            winEdit.show();
        }
    });

    var expireAuthorBtn = new Ext.Button({
        text: 'Expire selected',
        disabled: true,
        handler: function() {
            var toDelete = [];
            var items = store.data.items;
            for (var i = 0; i < items.length; i++) {
                if (items[i].data.checked) {
                    var id = items[i].id;
                    toDelete.push(id)
                }
            }
            Ext.Ajax.request({
                url: 'expire_authors',
                method: 'POST',
                params: {
                    toExpireArray: toDelete
                },
                success: function(response, opts) {
                    refreshGrid();
                },
            });
        }
    });

    var hireAuthorBtn = new Ext.Button({
        text: 'Hire selected',
        disabled: true,
        handler: function() {
            var toHire = [];
            var items = store.data.items;
            for (var i = 0; i < items.length; i++) {
                if (items[i].data.checked) {
                    var id = items[i].id;
                    toHire.push(id)
                }
            }
            Ext.Ajax.request({
                url: 'hire_authors',
                method: 'POST',
                params: {
                    toHireArray: toHire
                },
                success: function(response, opts) {
                    refreshGrid()
                },
            });
        }
    });

    var grid = new Ext.grid.GridPanel({
        store: store,
        cm: cm,
        renderTo: 'panel',
        width: 600,
        height: 600,
        autoExpandColumn: 'name', // column with this id will be expanded
        title: 'Edit Authors?',
        frame: true,
        tbar: [
            addAuthorBtn,
            editAuthorBtn,
            expireAuthorBtn,
            hireAuthorBtn
        ],
    });

    var winAdd = new Ext.Window({
        title: 'Add a new author',
        width: 400,
        height: 150,
        layout: 'fit',
        closeAction: 'hide', //default value is close

        shadow: false,
        resizable: false,
        draggable: true,
        closable: false,
        modal: true,

        items: {
            xtype: 'form',
            itemId: 'form',
            id: 'ajaxAddAuthorForm',
            items: [{
                xtype: 'textfield',
                itemId: 'authorNameFld',
                width: 250,
                fieldLabel: 'Name',
                maxLength: 30,
                allowBlank: false,
                maxLengthText: 'Max length of the name is 30 sym.',
                blankText: 'Name shuldn\'t be empty',
                msgTarget: 'under',
                emptyText: 'Enter a name:',
                enableKeyEvents: true,

                listeners: {
                    blur: function() {
                        Ext.MessageBox.alert('OnBlur!')
                    }
                }

            }, {
                xtype: 'checkbox',
                itemId: 'expiredChbx',
                fieldLabel: 'Expired'
            }],

            buttons: [{
                text: 'Add',
                itemId: 'addAuthorBtn',
                handler: function() {
                    var inputExpired = winAdd.getComponent('form').getComponent('expiredChbx').getValue();
                    var inputName = winAdd.getComponent('form').getComponent('authorNameFld').getValue();
                    var form = winAdd.getComponent('form').getForm();

                    if (form.isValid()) {
                        Ext.Ajax.request({
                            url: 'add_author',
                            method: 'POST',
                            //form: 'ajaxAddAuthorForm',
                            params: {
                                name: inputName,
                                expired: inputExpired
                            },
                            success: function(response, opts) {
                                winAdd.getComponent('form').getForm().reset();
                                winAdd.hide();
                                refreshGrid();
                            },
                        });
                    } else {
                        Ext.MessageBox.alert('Wrong input!', 'The name is incorrect!')
                    }
                }
            }, {
                text: 'Cancel',
                handler: function() {
                    winAdd.getComponent('form').getForm().reset();
                    winAdd.hide();
                }
            }]
        },
    });


});