package com.epam.ivanou.layouts;

import com.epam.ivanou.pages.DetailedPage;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.models.CommentModel;
import org.apache.log4j.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.datetime.markup.html.basic.DateLabel;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.resource.PackageResourceReference;

public class CommentPanel extends Panel {

    private static final Logger log = Logger.getLogger(CommentPanel.class);

    private Long commentId = null;

    public CommentPanel(String id, CommentModel model) {
        super(id, model);
        log.debug("CommentPanel constructor");

        CompoundPropertyModel cModel = CompoundPropertyModel.of(getDefaultModel());
        commentId = ((Comment) getDefaultModelObject()).getId();

        add(DateLabel.forDatePattern("creationDate", cModel.bind("creationDate"),
                "MM/dd/yyyy"));
        add(new MultiLineLabel("text", cModel.bind("text")));

        AjaxLink deleteCommentButton = new AjaxLink("deleteCommentButton") {

            @Override
            public void onClick(AjaxRequestTarget target) {
                log.debug("Delete button was pressed");
                try {
                    ((DetailedPage) getPage()).deleteComment(commentId);
                    target.add(getPage());
                } catch (ServiceException e) {
                    log.error("Error while deleting comment with id = " + commentId);
                }
            }
            @Override
            protected void onConfigure() {
                super.onConfigure();
                setVisible( AuthenticatedWebSession.get().isSignedIn() );
            }
        };
        add(deleteCommentButton);

        PackageResourceReference resourceReference =
                new PackageResourceReference(getClass(), "resources/delete_button.png");
        deleteCommentButton.add(new Image("deleteCommentPic", resourceReference) {
            @Override
            protected void onConfigure() {
                super.onConfigure();
                setVisible(AuthenticatedWebSession.get().isSignedIn());
            }
        });
        detachModels();
    }

    @Override
    protected void onRender() {
        super.onRender();
        log.debug("CommentPanel onRender()");
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        log.debug("CommentPanel onInitialize()");
    }
}
