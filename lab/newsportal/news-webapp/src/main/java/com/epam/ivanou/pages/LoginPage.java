package com.epam.ivanou.pages;

import com.epam.ivanou.Mode;
import com.epam.ivanou.layouts.Template;
import com.epam.ivanou.sessions.SecureWicketSession;
import org.apache.log4j.Logger;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;

public class LoginPage extends Template {

    private static final Logger log = Logger.getLogger(LoginPage.class);

    public LoginPage() {
        super();
        log.debug("LoginPage constructor");
        SecureWicketSession.get().signOut();
        WebMarkupContainer menuStub = new WebMarkupContainer("menuPanel");
        menuStub.setVisible(false);
        addOrReplace(menuStub);
        add(new LoginForm("loginForm"));
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();
        log.debug("LoginPage onConfigure()");
    }

    @Override
    protected void onInitialize() {
        log.debug("LoginPage onInitialize()");
        super.onInitialize();
    }

    @Override
    protected void onRender() {
        super.onRender();
        log.debug("LoginPage onRender()");
    }

    private static class LoginForm extends StatelessForm<Void> {

        private static final Logger log = Logger.getLogger(LoginForm.class);
        private String username;
        private String password;

        @SuppressWarnings("unchecked")
        public LoginForm(String id) {
            super(id);
            log.debug("LoginForm constructor");
            setModel(new CompoundPropertyModel(this));
            add(new RequiredTextField<String>("username"));
            add(new PasswordTextField("password"));
            add(new FeedbackPanel("feedback"));
        }

        @Override
        protected void onSubmit() {
            log.debug("LoginForm onSubmit()");
            AuthenticatedWebSession session = SecureWicketSession.get();
            if (session.signIn(username, password)) {
                    log.info("Username and password are correct. Redirecting to " +
                            "HomePage with ROLE_ADMIN");
                setResponsePage(HomePage.class);
            } else {
                log.debug("Login failed due to invalid credentials");
                error("Login failed due to invalid credentials");
            }
        }

        @Override
        protected void onConfigure() {
            super.onConfigure();
            log.debug("LoginForm onConfigure()");
        }

        @Override
        protected void onInitialize() {
            log.debug("LoginForm onInitialize()");
            super.onInitialize();
        }

        @Override
        protected void onRender() {
            super.onRender();
            log.debug("LoginForm onRender()");
        }
    }
}
