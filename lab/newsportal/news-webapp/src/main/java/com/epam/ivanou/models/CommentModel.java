package com.epam.ivanou.models;

import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.service.INewsManagementService;
import com.epam.ivanou.utils.NewsValueObject;
import org.apache.log4j.Logger;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class CommentModel extends LoadableDetachableModel {

    private static final Logger log = Logger.getLogger(CommentModel.class);

    private Long commentId;

    @SpringBean(name = "newsManagementService")
    private transient INewsManagementService service;

    public CommentModel(Comment comment) {
        super(comment);
        Injector.get().inject(this);
        this.commentId = comment.getId();
    }

    public CommentModel(Long commentId) {
        super();
        Injector.get().inject(this);
        this.commentId = commentId;
        log.debug("Inside CommentModel#CommentModel(Long commentId), commentId = " + commentId);
    }


    @Override
    protected Object load() { //if the object is not attached then it will be loaded when getObject()  is invoked
        if (commentId == null) {
            log.debug("Object is not attached, new empty instance NewsValueObject() is created");
            return new Comment();
        }

        try {
            log.debug("Object is not attached, new instance from database is created.");
            Comment object = service.getCommentById(commentId);
            return object;
        } catch (ServiceException e) {
            log.error("Exception during retrieving the NewsValueObject from the database.", e);
        }
        return new Comment();
    }

    public void setService(INewsManagementService service) {
        this.service = service;
    }
}
