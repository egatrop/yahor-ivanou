package com.epam.ivanou.layouts;

import org.apache.log4j.Logger;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;

import com.epam.ivanou.pages.AuthorsPage;
import com.epam.ivanou.pages.HomePage;
import com.epam.ivanou.pages.TagsPage;

public class MenuPanel extends Panel {

    private static final Logger log = Logger.getLogger(MenuPanel.class);

    public MenuPanel(String id) {
        super(id);
        log.debug("MenuPanel constructor");
        add(new BookmarkablePageLink("homePage", HomePage.class));
        add(new BookmarkablePageLink("tagsPage", TagsPage.class));
        add(new BookmarkablePageLink("authorsPage", AuthorsPage.class));
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();
        log.debug("MenuPanel onConfigure()");
    }

    @Override
    protected void onInitialize() {
        log.debug("MenuPanel onInitialize()");
        super.onInitialize();
    }

    @Override
    protected void onRender() {
        super.onRender();
        log.debug("MenuPanel onRender()");
    }
}
