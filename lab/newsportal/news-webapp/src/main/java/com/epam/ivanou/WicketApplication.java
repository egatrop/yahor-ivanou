package com.epam.ivanou;

import com.epam.ivanou.pages.AuthorsPage;
import com.epam.ivanou.pages.HomePage;
import com.epam.ivanou.pages.LoginPage;
import com.epam.ivanou.pages.TagsPage;
import com.epam.ivanou.sessions.SecureWicketSession;
import org.apache.log4j.Logger;
import org.apache.wicket.Session;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.devutils.stateless.StatelessChecker;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.settings.IRequestCycleSettings;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


/**
 * Application object for your web application.
 * If you want to run this application without deploying, run the Start class.
 *
 * @see com.epam.ivanou.Start#main(String[])
 */
public class WicketApplication extends AuthenticatedWebApplication implements ApplicationContextAware {

    private static final Logger log = Logger.getLogger(WicketApplication.class);
    private ApplicationContext applicationContext;
    private boolean isInitialized = false;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public Class<? extends WebPage> getHomePage() {
        return HomePage.class;
    }

    @Override
    public void init() {
        if (!isInitialized) {
            super.init();
            isInitialized = true;
            mountPage("/login", LoginPage.class);
            mountPage("/tags", TagsPage.class);
            mountPage("/authors", AuthorsPage.class);
            getMarkupSettings().setStripWicketTags(true);
            initSpringInjector();
            getComponentPostOnBeforeRenderListeners().add(new StatelessChecker());
            getRequestCycleSettings().
                    setRenderStrategy(IRequestCycleSettings.RenderStrategy.REDIRECT_TO_BUFFER);
        }
    }

    @Override
    public Session newSession(Request request, Response response) {
        return new SecureWicketSession(request);
    }

    @Override
    protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
        return SecureWicketSession.class;
    }

    @Override
    protected Class<? extends WebPage> getSignInPageClass() {
        return LoginPage.class;
    }

    protected void initSpringInjector() {
        if (applicationContext == null) {
            getComponentInstantiationListeners().add(new SpringComponentInjector(this));
        } else {
            getComponentInstantiationListeners().add(new SpringComponentInjector(this, applicationContext));
        }
    }

}
