package com.epam.ivanou.filters;

import com.epam.ivanou.entity.Author;
import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.service.INewsManagementService;
import com.epam.ivanou.service.impl.jdbc.NewsManagementJDBCService;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

public class AuthorsFilter implements Filter {

    private static final Logger log = Logger.getLogger(AuthorsFilter.class);

    private INewsManagementService service;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ApplicationContext ctx = WebApplicationContextUtils
                .getRequiredWebApplicationContext(filterConfig.getServletContext());
        this.service = (INewsManagementService)ctx.getBean("newsManagementService");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        log.debug("Inside authorsFilter");
        HttpServletRequest req = (HttpServletRequest) request;
        if (req.getRequestURI().equals("/newsportal/authors-data")) {
            respondWithJson(request, response, chain);
        }
        if (req.getRequestURI().equals("/newsportal/add_author")) {
            addAuthor(req.getParameter("name"), req.getParameter("expired"));
        }
        if (req.getRequestURI().equals("/newsportal/edit_author")) {
            editAuthor(req.getParameter("id"), req.getParameter("name"),
                    req.getParameter("expired"));
        }
        if (req.getRequestURI().equals("/newsportal/expire_authors")) {
            expire(req.getParameterValues("toExpireArray"));
        }
        if (req.getRequestURI().equals("/newsportal/hire_authors")) {
            hire(req.getParameterValues("toHireArray"));
        }
    }

    private void editAuthor(String id, String name, String expired) {
        Author author = new Author();
        author.setId(Long.valueOf(id));
        author.setName(name);
        if(expired.equals("true")) {
            author.setExpiredDate(Timestamp.valueOf(LocalDateTime.now()));
        } else {
            author.setExpiredDate(null);
        }
        try {
            service.editAuthor(author);
        } catch (ServiceException e) {
            log.error("Exception during expiring the author.", e);
        }
    }

    private void hire(String[] toExpireArrays) {
        for (int i = 0; i < toExpireArrays.length; i++) {
            Long id = Long.valueOf(toExpireArrays[i]);
            try {
                Author author = service.getAuthorById(id);
                author.setExpiredDate(null);
                service.editAuthor(author);
            } catch (ServiceException e) {
                log.error("Exception during hiring the author.", e);
            }
        }
    }

    private void expire(String[] toDeleteArray) {
        for (int i = 0; i < toDeleteArray.length; i++) {
            Long id = Long.valueOf(toDeleteArray[i]);
            try {
                Author author = service.getAuthorById(id);
                service.expireAuthor(author);
            } catch (ServiceException e) {
                log.error("Exception during expiring the author.", e);
            }
        }
    }

    private void addAuthor(String name, String expired) {
        Author author = new Author();
        author.setName(name);
        if(expired.equals("true")) {
            author.setExpiredDate(Timestamp.valueOf(LocalDateTime.now()));
        }
        try {
            service.addAuthor(author);
        } catch (ServiceException e) {
            log.error("Exception during inserting the author to the database.", e);
        }
    }


    private void respondWithJson(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            List<Author> authors = service.getAllAuthors();
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            PrintWriter out = response.getWriter();

            JSONObject json = new JSONObject();
            JSONArray records = new JSONArray();
            json.put("records", records);

            for (Author author : authors) {
                JSONObject record = new JSONObject();
                record.put("id", author.getId());
                record.put("name", author.getName());
                boolean expired = false;
                if (author.getExpiredDate() != null) {
                    expired = true;
                }
                record.put("expired", expired);
                records.put(record);
            }
            out.print(json.toString());
        } catch (ServiceException e) {
            log.error("Exception during retrieving all the authors from the database.", e);
            HttpServletResponse res = (HttpServletResponse)response;
            res.sendError(500);
        }
    }

    @Override
    public void destroy() {
    }

}
