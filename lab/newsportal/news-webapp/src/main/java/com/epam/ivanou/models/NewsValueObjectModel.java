package com.epam.ivanou.models;

import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.service.INewsManagementService;
import com.epam.ivanou.utils.NewsValueObject;
import org.apache.log4j.Logger;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class NewsValueObjectModel extends LoadableDetachableModel {

    private static final Logger log = Logger.getLogger(NewsValueObjectModel.class);

    private Long newsId;

    @SpringBean(name = "newsManagementService")
    private transient INewsManagementService service;

    public NewsValueObjectModel(NewsValueObject valueObject) {
        super(valueObject);
        Injector.get().inject(this);
        this.newsId = valueObject.getNews().getId();
    }

    public NewsValueObjectModel(Long newsId) {
        super();
        Injector.get().inject(this);
        this.newsId = newsId;
        log.debug("Inside NewsValueObjectModel#NewsValueObjectModel(Long newsId), newsId = " + newsId);
    }


    @Override
    protected Object load() { //if the object is not attached then it will be loaded when getObject()  is invoked
        if (newsId == null) {
            log.debug("Object is not attached, new empty instance NewsValueObject() is created");
            return new NewsValueObject();
        }

        try {
            log.debug("Object is not attached, new instance from database is created.");
            NewsValueObject object = service.getNewsById(newsId);
            return object;
        } catch (ServiceException e) {
            log.error("Exception during retrieving the NewsValueObject from the database.", e);
        }
        return new NewsValueObject();
    }

    public void setService(INewsManagementService service) {
        this.service = service;
    }
}
