package com.epam.ivanou.providers;

import com.epam.ivanou.models.NewsValueObjectModel;
import com.epam.ivanou.utils.NewsValueObject;
import org.apache.log4j.Logger;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class NewsDataProvider implements IDataProvider {

    private static final Logger log = Logger.getLogger(NewsDataProvider.class);
    private transient List<NewsValueObject> list;
    private List<Long> listId;

    public NewsDataProvider() {
        this(Collections.<NewsValueObject>emptyList());
    }

    public NewsDataProvider(List<NewsValueObject> list) {
        if (list == null) {
            throw new IllegalArgumentException("argument [list] cannot be null");
        }
        listId = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            listId.add(list.get(i).getNews().getId());
        }
        this.list = list;
    }

    protected List<NewsValueObject> getData() {
        if (list == null) {
            list = new ArrayList<>();
            for (Long id : listId) {
                list.add((NewsValueObject) new NewsValueObjectModel(id).getObject());
            }
        }
        return list;
    }

    public void setData(List<NewsValueObject> list) {
        this.list = list;
        this.listId = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            listId.add(list.get(i).getNews().getId());
        }
    }

    public Iterator<NewsValueObject> iterator(final int first, final int count) {
        List<NewsValueObject> list = getData();
        int toIndex = first + count;
        if (toIndex > list.size()) {
            toIndex = list.size();
        }
        return list.subList(first, toIndex).listIterator();
    }

    public int size() {
        return getData().size();
    }

    public void detach() {
    }

    @Override
    public IModel model(Object object) {
        return new NewsValueObjectModel(((NewsValueObject) object).getNews().getId());
    }
}
