package com.epam.ivanou.pages;

import com.epam.ivanou.Mode;
import com.epam.ivanou.entity.Comment;
import com.epam.ivanou.exception.ServiceException;
import com.epam.ivanou.layouts.CommentForm;
import com.epam.ivanou.layouts.CommentPanel;
import com.epam.ivanou.layouts.NewsPanel;
import com.epam.ivanou.layouts.Template;
import com.epam.ivanou.models.CommentModel;
import com.epam.ivanou.models.NewsValueObjectModel;
import com.epam.ivanou.service.INewsManagementService;
import com.epam.ivanou.utils.NewsValueObject;
import org.apache.log4j.Logger;
import org.apache.wicket.Component;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.MarkupStream;
import org.apache.wicket.markup.html.TransparentWebMarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.StatelessLink;
import org.apache.wicket.markup.html.pages.ExceptionErrorPage;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DetailedPage extends Template implements Modeable {

    private static final Logger log = Logger.getLogger(DetailedPage.class);

    private ArrayList<Long> allNewsIdCache;
    private Long nextNewsId;
    private Long prevNewsId;
    private Long currentNewsId;
    private Component refreshingViewWrapper;

    @SpringBean(name = "newsManagementService")
    private INewsManagementService service;

    public DetailedPage(PageParameters parameters) {
        super();
        log.debug("DetailedPage constructor");
        if (!AuthenticatedWebSession.get().isSignedIn()) {
            WebMarkupContainer menuStub = new WebMarkupContainer("menuPanel");
            menuStub.setVisible(false);
            addOrReplace(menuStub);
        }
        currentNewsId = Long.valueOf(parameters.get("newsId").toString());
        NewsValueObject newsValueObject;
        try {
            newsValueObject = service.getNewsById(currentNewsId);
        } catch (ServiceException e) {
            log.error("Couldn't retrieve news from the database", e);
            setResponsePage(ExceptionErrorPage.class);
            throw new RuntimeException(e);
        }
        setDefaultModel(new NewsValueObjectModel(newsValueObject));

        add(new NewsPanel("newsItem",
                (NewsValueObjectModel) (getDefaultModel()),
                getModes()));
        //List of comments
        //Wrapper for comments to enable Ajax refreshing only for refreshingView (comments)
        refreshingViewWrapper = new TransparentWebMarkupContainer("commentsWrapper");
        refreshingViewWrapper.setOutputMarkupId(true);
        add(refreshingViewWrapper);
        RefreshingView refreshingView = new RefreshingView<Comment>("comments") {
            @Override
            protected Iterator<IModel<Comment>> getItemModels() {
                final List<IModel<Comment>> models = new ArrayList<>();
                for (Comment comment : ((NewsValueObject) getPage()
                        .getDefaultModel().getObject()).getComments()) {
                    models.add(new CommentModel(comment));
                }
                return models.iterator();
            }

            @Override
            protected void populateItem(Item<Comment> item) {
                item.add(new CommentPanel("commentPanel",
                        (CommentModel) item.getModel()));
            }

            @Override
            protected void onInitialize() {
                log.debug("RefreshingView onInitialize()");
                super.onInitialize();
            }

            @Override
            protected void onConfigure() {
                super.onConfigure();
                log.debug("RefreshingView onConfigure()");
            }


        };
        refreshingView.setOutputMarkupId(true);
        add(refreshingView);

        CommentForm form = new CommentForm("commentForm", currentNewsId);
        add(form);
        form.setOutputMarkupId(true);

        add(new StatelessLink("prev") {
            @Override
            protected void onConfigure() {
                super.onConfigure();

                if (prevNewsId == null) {
                    this.setVisible(false);
                }
            }

            @Override
            public void onClick() {
                setResponsePage(DetailedPage.class, new PageParameters().add("newsId", prevNewsId));
            }

            @Override
            protected void onComponentTag(ComponentTag tag) {
                if (AuthenticatedWebSession.get().isSignedIn()) {
                    tag.append("class", "positionedLeft250", " ");
                }
                super.onComponentTag(tag);
            }
        });

        add(new StatelessLink("next") {
            @Override
            protected void onConfigure() {
                super.onConfigure();
                if (nextNewsId == null) {
                    this.setVisible(false);
                }
            }

            @Override
            public void onClick() {
                setResponsePage(DetailedPage.class, new PageParameters().add("newsId", nextNewsId));
            }
        });

        add(new StatelessLink("back") {

            private Serializable attribute = getSession().getAttribute("backPage");

            @Override
            protected void onConfigure() {
                super.onConfigure();
                if (attribute == null) {
                    this.setVisible(false);
                }
            }

            @Override
            public void onClick() {
                setResponsePage((HomePage) attribute);
            }
        });
    }

    public Component getRefreshingViewWrapper() {
        return refreshingViewWrapper;
    }

    @Override
    public Mode[] getModes() {
        if (AuthenticatedWebSession.get().isSignedIn()) {
            return new Mode[]{Mode.FULL, Mode.ADMIN};
        } else {
            return new Mode[]{Mode.FULL, Mode.CLIENT};
        }
    }

    @Override
    protected void onInitialize() {
        log.debug("DetailedPage onInitialize()");
        super.onInitialize();
        ArrayList<Long> newsIdCache = (ArrayList<Long>) getSession().getAttribute("newsIdCache");
        //If this page is get as the bookmark, then allNewsIdCache is empty and should be retrieved
        //from database to get newsId for 'next' and 'previous' links.
        if (newsIdCache != null) {
            this.allNewsIdCache = newsIdCache;
        } else {
            initNewsIdCacheFromDataBase();
        }
        initNextPrevNewsId();
    }

    private void initNextPrevNewsId() {
        for (int i = 0; i < allNewsIdCache.size(); i++) {
            if (currentNewsId.equals(allNewsIdCache.get(i))
                    && i != 0) {
                prevNewsId = allNewsIdCache.get(i - 1);
            }
            if (currentNewsId.equals(allNewsIdCache.get(i))
                    && i != (allNewsIdCache.size() - 1)) {
                nextNewsId = allNewsIdCache.get(i + 1);
            }
        }
    }

    private void initNewsIdCacheFromDataBase() {
        List<NewsValueObject> valueObjects;
        try {
            valueObjects = service.getAllNews();
            allNewsIdCache = new ArrayList<>();
            for (NewsValueObject object : valueObjects) {
                allNewsIdCache.add(object.getNews().getId());
            }
            getSession().setAttribute("newsIdCache", allNewsIdCache);
        } catch (ServiceException e) {
            log.error("Exception during retrieving all the news.", e);
        }
    }

    public void deleteComment(Long commentId) throws ServiceException {
        log.debug("Deleting comment from database, id = " + commentId);
        service.deleteComment(commentId);
    }

    public void addComment(Comment comment) throws ServiceException {
        log.debug("Adding comment to database, id = " + comment);
        service.addComment(comment);
    }

    @Override
    protected void onRender() {
        super.onRender();
        log.debug("DetailedPage onRender()");
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();
        log.debug("DetailedPage onConfigure()");
    }

    @Override
    protected void onAfterRender() {
        super.onAfterRender();
        log.debug("DetailedPage onAfterRender()");
    }

    @Override
    protected void onBeforeRender() {
        super.onBeforeRender();
        log.debug("DetailedPage onBeforeRender()");
    }

    @Override
    protected void onDetach() {
        super.onDetach();
        log.debug("DetailedPage onDetach()");
    }

    @Override
    protected void onAfterRenderChildren() {
        super.onAfterRenderChildren();
        log.debug("DetailedPage onAfterRenderChildren()");
    }

    @Override
    protected void onMarkupAttached() {
        super.onMarkupAttached();
        log.debug("DetailedPage onMarkupAttached()");
    }

    @Override
    protected void onComponentTag(ComponentTag tag) {
        super.onComponentTag(tag);
        log.debug("DetailedPage onComponentTag()");
    }

    @Override
    public void onComponentTagBody(MarkupStream markupStream, ComponentTag openTag) {
        super.onComponentTagBody(markupStream, openTag);
        log.debug("DetailedPage onComponentTagBody()");
    }

    @Override
    protected void onModelChanged() {
        super.onModelChanged();
        log.debug("DetailedPage onModelChanged()");
    }

    @Override
    protected void onModelChanging() {
        super.onModelChanging();
        log.debug("DetailedPage onModelChanging()");
    }

    @Override
    public void onEvent(IEvent<?> event) {
        super.onEvent(event);
        log.debug("DetailedPage onEvent()");
    }
}
