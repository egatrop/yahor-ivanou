package com.epam.ivanou.pages;

import com.epam.ivanou.layouts.Template;
import com.epam.ivanou.service.INewsManagementService;
import org.apache.log4j.Logger;
import org.apache.wicket.markup.html.IHeaderResponse;
import org.apache.wicket.request.resource.ContextRelativeResource;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class AuthorsPage extends Template {

    private static final Logger log = Logger.getLogger(AuthorsPage.class);

    @SpringBean(name = "newsManagementService")
    private INewsManagementService service;

    public AuthorsPage() {
        super();
        log.debug("AuthorsPage constructor");
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        response.renderJavaScriptReference("./res/ext-3.4.1/adapter/ext/ext-base-debug.js");
        response.renderJavaScriptReference("./res/ext-3.4.1/ext-all-debug-w-comments.js");
        response.renderJavaScriptReference("./res/CheckColumn.js");
        response.renderJavaScriptReference(new PackageResourceReference(HomePage.class,
                "resources/authors-grid.js"));
        response.renderCSSReference("./res/ext-3.4.1/resources/css/ext-all.css");
    }

}
